﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jims.Profile;


namespace Jims.InputSystem
{
	public partial class Jims_CameraController : MonoBehaviour
	{
		#region

		public Jims_CameraControllerProfile Profile = null;

		#endregion

		#region

		//USED WITH EDITOR!
		public bool showInputSettings = false;
		public bool showLimitSettings = false;
		public bool showHelp = true;

		#endregion

		#region

		[Header ("Inputs")]
		[SerializeField]
		public InputType InputSystem;
		public enum  InputType
		{
			Windows,
			Mobile}
		;

		#endregion

		#region

		[SerializeField]
		public MovementMode MoveMode;
		public enum  MovementMode
		{
			Keyboard,
			ClickDrag,
			both}
		;

		#endregion

		#region

		[SerializeField]
		[Tooltip ("Mouse Scroll Wheel Input Name!")]
		public string MouseScrollWheelInputName = "Mouse ScrollWheel";
		[SerializeField]
		[Tooltip ("Zoom In Input Key!")]
		public KeyCode ZoomInKey;
		[SerializeField]
		[Tooltip ("Zoom Out Input Key!")]
		public KeyCode ZoomOutKey;
		[SerializeField]
		[Tooltip ("Drag Camera Key!")]
		public KeyCode CameraDragKey;
		[SerializeField]
		[Tooltip ("Reset Camera Key!")]
		public KeyCode CameraResetKey;

		#endregion

		#region

		[Header ("Settings")]
		[SerializeField]
		[Range (0f, 25f)]
		[Tooltip ("Set Camera default height!")]
		public float CamHeight = 8f;

		[SerializeField]
		[Range (0f, 25f)]
		[Tooltip ("If Camera is orthographic, set size value!")]
		public float CamZoomMin = 6f;

		[SerializeField]
		[Range (0f, 25f)]
		[Tooltip ("If Camera is orthographic, set size value!")]
		public float CamZoomMax = 10f;

		[SerializeField]
		[Range (1f, 120f)]
		[Tooltip ("If Camera is NOT orthographic, set FOV value!")]
		public float CamFOVMin = 40f;

		[SerializeField]
		[Range (1f, 120f)]
		[Tooltip ("If Camera is NOT orthographic, set FOV value!")]
		public float CamFOVMax = 60f;

		[SerializeField]
		[Range (-100f, 100f)]
		[Tooltip ("Set backwards limit!")]
		public float CamVerticalMin = -20f;

		[SerializeField]
		[Range (-100f, 100f)]
		[Tooltip ("Set forwards limit!")]
		public float CamVerticalMax = 65f;

		[SerializeField]
		[Range (-100f, 100f)]
		[Tooltip ("Set left limit!")]
		public float CamHorizontalMin = -20f;

		[SerializeField]
		[Range (-100f, 100f)]
		[Tooltip ("Set right limit!")]
		public float CamHorizontalMax = 15f;

		[SerializeField]
		[Range (0.1f, 2f)]
		[Tooltip ("Set camera move speed multi!")]
		public float CamSpeed = 0.3f;

		[SerializeField]
		[Range (0.1f, 10f)]
		[Tooltip ("Divide by Multi, High is slower speed!")]
		public float CamHeightSpeedMulti = 2f;

		#endregion

		#region

		[Header ("Private")]
		protected Vector3 ResetCamera;
		protected Vector3 CamOrigin;
		protected Vector3 Difference;
		protected Camera m_MainCam = null;
		public Camera MainCam {
			get {
				if (m_MainCam == null)
					m_MainCam = Camera.main;
				return m_MainCam;
			}
		}

		#endregion


		/// <summary>
		/// Load Setting's from Profile(If any), and create a Camera Reset Point!
		/// </summary>
		public virtual void Start ()
		{
			//Load data from Profile is any!
			if (Profile != null)
				Profile.Load (this);
			
			ResetCamera = MainCam.transform.position;
		}

		/// <summary>
		/// Load Setting's from Profile!
		/// </summary>
		public virtual void LoadFromProfile ()
		{
			if (Profile == null)
				return;

			Profile.Load (this);
		}

		/// <summary>
		/// Save Changes to Profile!
		/// </summary>
		public virtual void SaveToProfile ()
		{
			if (Profile == null)
				return;

			Profile.Save (this);
		}

		/// <summary>
		/// Calculates Camera Move Speed!
		/// </summary>
		float CamSpeedValue ()
		{
			return CamSpeed * 100f;
		}

		/// <summary>
		/// Calculates Camera Position Vertical!
		/// </summary>
		float CamPosZValue ()
		{
			return MainCam.transform.position.z;
		}

		/// <summary>
		/// Calculates Camera Position Horizontal!
		/// </summary>
		float CamPosXValue ()
		{
			return MainCam.transform.position.x;
		}

		/// <summary>
		/// Calculates Camera Height!
		/// </summary>
		float CamPosYValue ()
		{
			return CamHeight;
		}

		/// <summary>
		/// Calculates Camera Position!
		/// </summary>
		Vector3 CameraPosition ()
		{
			return new Vector3 (CamPosXValue (), CamPosYValue (), CamPosZValue ());
		}

		/// <summary>
		/// Calculates Mouse Position!
		/// </summary>
		Vector3 MousePosition ()
		{
			return MainCam.ScreenToWorldPoint (Input.mousePosition);
		}

		/// <summary>
		/// Used by Dragging to calculate position!
		/// </summary>
		Vector3 GetOrigin ()
		{
			return new Vector3 (MousePosition ().x, CamPosYValue (), MousePosition ().z);
		}

		/// <summary>
		/// Used by Dragging to calculate position!
		/// </summary>
		Vector3 GetDifference ()
		{
			return GetOrigin () - CameraPosition ();
		}

		/// <summary>
		/// Use LateUpdate as it's not very important!
		/// </summary>
		public virtual void LateUpdate ()
		{
			//Don't Continue if Time is paused!
			if (Time.deltaTime == 0f)
				return;
			//Don't Continue if Camera is Missing!
			if (MainCam == null)
				return;

			//INPUTS!
			//Is Windows Build
			switch (InputSystem) {
			case InputType.Windows:
				{
					//RESET CAMERA TO START POSITION!
					//Reset Camera position On Right Click
					if (Input.GetKeyDown (CameraResetKey))
						MainCam.transform.position = ResetCamera;

					//ZOOMING!
					//Wait for Inputs and Zoom the Camera Up/Down!
					if (Input.GetAxis (MouseScrollWheelInputName) > 0f || Input.GetKeyDown (ZoomInKey)) {
						if (MainCam.orthographic)
							MainCam.orthographicSize -= (CamSpeedValue () / CamHeightSpeedMulti) * Time.deltaTime;
						else
							MainCam.fieldOfView -= (CamSpeedValue () * 2 / CamHeightSpeedMulti) * Time.deltaTime;
					} else if (Input.GetAxis (MouseScrollWheelInputName) < 0f || Input.GetKeyDown (ZoomOutKey)) {
						if (MainCam.orthographic)
							MainCam.orthographicSize += (CamSpeedValue () / CamHeightSpeedMulti) * Time.deltaTime;
						else
							MainCam.fieldOfView += (CamSpeedValue () * 2 / CamHeightSpeedMulti) * Time.deltaTime;
					}

					//CAMERA LIMITS!
					//Set Min/Max Camera Positions Vertical!
					if (MainCam.transform.position.z >= CamVerticalMax)
						MainCam.transform.position = new Vector3 (CamPosXValue (), CamPosYValue (), CamVerticalMax);
					if (MainCam.transform.position.z <= CamVerticalMin)
						MainCam.transform.position = new Vector3 (CamPosXValue (), CamPosYValue (), CamVerticalMin);			
					//Set Min/Max Camera Positions Horizontal!
					if (MainCam.transform.position.x >= CamHorizontalMax)
						MainCam.transform.position = new Vector3 (CamHorizontalMax, CamPosYValue (), CamPosZValue ());
					if (MainCam.transform.position.x <= CamHorizontalMin)
						MainCam.transform.position = new Vector3 (CamHorizontalMin, CamPosYValue (), CamPosZValue ());
					//Set Camera pos y limit
					if (MainCam.transform.position.y >= CamPosYValue ())
						MainCam.transform.position = new Vector3 (CamPosXValue (), CamPosYValue (), CamPosZValue ());
					if (MainCam.transform.position.y <= CamPosYValue ())
						MainCam.transform.position = new Vector3 (CamPosXValue (), CamPosYValue (), CamPosZValue ());
					//Set Min/Max Camera Zoom limits!
					if (MainCam.orthographic) {
						if (MainCam.orthographicSize >= CamZoomMax)
							MainCam.orthographicSize = CamZoomMax;
						if (MainCam.orthographicSize <= CamZoomMin)
							MainCam.orthographicSize = CamZoomMin;
					} else {
						if (MainCam.fieldOfView >= CamFOVMax)
							MainCam.fieldOfView = CamFOVMax;
						if (MainCam.fieldOfView <= CamFOVMin)
							MainCam.fieldOfView = CamFOVMin;
					}
					
					switch (MoveMode) {
					case MovementMode.Keyboard:
						{
							//USING KEYBOARD TO MOVE!
							//Wait for Inputs and Move the Camera Vertical!
							if (Input.GetAxis ("Vertical") < 0f)
								MainCam.transform.position += Vector3.back * CamSpeedValue () * Time.deltaTime;
							else if (Input.GetAxis ("Vertical") > 0f)
								MainCam.transform.position += Vector3.forward * CamSpeedValue () * Time.deltaTime;

							//Wait for Inputs and Move the Camera Horizontal!
							if (Input.GetAxis ("Horizontal") < 0f)
								MainCam.transform.position += Vector3.left * CamSpeedValue () * Time.deltaTime;
							else if (Input.GetAxis ("Horizontal") > 0f)
								MainCam.transform.position += Vector3.right * CamSpeedValue () * Time.deltaTime;
						}
						break;
					case MovementMode.ClickDrag:
						{
							//USING CLICK DRAG TO MOVE!
							if (Input.GetKeyDown (CameraDragKey)) {
								CamOrigin = GetOrigin ();
							}
							if (Input.GetKey (CameraDragKey)) {
								Difference = GetDifference ();
								MainCam.transform.position = CamOrigin - Difference;
							}
						}
						break;
					case MovementMode.both:
						{
							//USING KEYBOARD TO MOVE!
							//Wait for Inputs and Move the Camera Vertical!
							if (Input.GetAxis ("Vertical") < 0f)
								MainCam.transform.position += Vector3.back * CamSpeedValue () * Time.deltaTime;
							else if (Input.GetAxis ("Vertical") > 0f)
								MainCam.transform.position += Vector3.forward * CamSpeedValue () * Time.deltaTime;

							//Wait for Inputs and Move the Camera Horizontal!
							if (Input.GetAxis ("Horizontal") < 0f)
								MainCam.transform.position += Vector3.left * CamSpeedValue () * Time.deltaTime;
							else if (Input.GetAxis ("Horizontal") > 0f)
								MainCam.transform.position += Vector3.right * CamSpeedValue () * Time.deltaTime;
							
							//USING CLICK DRAG TO MOVE!
							if (Input.GetKeyDown (CameraDragKey)) {
								CamOrigin = GetOrigin ();
							}
							if (Input.GetKey (CameraDragKey)) {
								Difference = GetDifference ();
								MainCam.transform.position = CamOrigin - Difference;
							}
						}
						break;
					}
				}
				break;
			case InputType.Mobile:
				{
					Debug.Log ("IS A MOBILE BUILD! WE NEED SOME [MOBILE INPUT SYSTEM] CODE!" + "Find this Script on! " + this.gameObject.name);
				}
				break;
			}
		}
	}
}