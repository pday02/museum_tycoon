﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;


namespace Jims.Profile
{
	[CustomEditor (typeof (Jims_CameraControllerProfile))]
	public class Jims_CameraProfileEditor : Editor
	{
		GUIStyle boxStyle;
		GUIStyle boxStyle2;

		Jims_CameraControllerProfile myTarget;

		Texture LogoTexture;

		SerializedObject serializedObj;

		SerializedProperty serializedPropertyInputSystem;
		SerializedProperty serializedPropertyMoveMode;
		SerializedProperty serializedPropertyZoomIn;
		SerializedProperty serializedPropertyZoomOut;
		SerializedProperty serializedPropertyDragKey;
		SerializedProperty serializedPropertyResetKey;


		void OnEnable ()
		{
			myTarget = (Jims_CameraControllerProfile) target;

			serializedObj = new SerializedObject (myTarget);

			serializedPropertyInputSystem = serializedObj.FindProperty ("InputSystem");
			serializedPropertyMoveMode = serializedObj.FindProperty ("MoveMode");
			serializedPropertyZoomIn = serializedObj.FindProperty ("ZoomInKey");
			serializedPropertyZoomOut = serializedObj.FindProperty ("ZoomOutKey");
			serializedPropertyDragKey = serializedObj.FindProperty ("CameraDragKey");
			serializedPropertyResetKey = serializedObj.FindProperty ("CameraResetKey");

			LogoTexture = Resources.Load ("Art/CameraProfileLogo") as Texture;
		}

		public override void OnInspectorGUI ()
		{
			myTarget = (Jims_CameraControllerProfile) target;
			#if UNITY_5_6_OR_NEWER
			serializedObj.UpdateIfRequiredOrScript ();
			#else
			serializedObj.UpdateIfDirtyOrScript ();
			#endif

			//Set up the box style
			if (boxStyle == null) {
				boxStyle = new GUIStyle (GUI.skin.box);
				boxStyle.normal.textColor = GUI.skin.label.normal.textColor;
				boxStyle.fontStyle = FontStyle.Bold;
				boxStyle.alignment = TextAnchor.UpperCenter;
			}

			if (boxStyle2 == null) {
				boxStyle2 = new GUIStyle (GUI.skin.label);
				boxStyle2.normal.textColor = GUI.skin.label.normal.textColor;
				boxStyle2.fontStyle = FontStyle.Bold;
				boxStyle2.alignment = TextAnchor.UpperCenter;		
			}

			// Begin
			GUILayout.BeginVertical ("", boxStyle);
			GUILayout.Space (10);

			//
			GUILayout.BeginVertical ("", boxStyle2);
			GUILayout.Label (LogoTexture, GUILayout.Width(350), GUILayout.Height(150));
			if(LogoTexture == null)
				EditorGUILayout.LabelField ("CAMERA PROFILE!", EditorStyles.boldLabel);
			EditorGUILayout.EndVertical ();

			// 
			GUILayout.BeginVertical ("", boxStyle2);
			myTarget.showInputSettings = EditorGUILayout.BeginToggleGroup ("Show Input Settings!", myTarget.showInputSettings);

			if (myTarget.showInputSettings) {				
				GUILayout.BeginVertical ("", boxStyle);
				GUILayout.Space (15);

				EditorGUILayout.PropertyField (serializedPropertyInputSystem, true);

				EditorGUILayout.PropertyField (serializedPropertyMoveMode, true);

				myTarget.MouseScrollWheelInputName = EditorGUILayout.TextField ("Mouse Scroll Wheel Input Name", myTarget.MouseScrollWheelInputName);

				EditorGUILayout.PropertyField (serializedPropertyZoomIn, true);

				EditorGUILayout.PropertyField (serializedPropertyZoomOut, true);

				EditorGUILayout.PropertyField (serializedPropertyDragKey, true);

				EditorGUILayout.PropertyField (serializedPropertyResetKey, true);

				GUILayout.Space (15);	
				EditorGUILayout.EndVertical ();
			}

			EditorGUILayout.EndToggleGroup ();
			EditorGUILayout.EndVertical ();
			//

			// 
			GUILayout.BeginVertical ("", boxStyle2);
			myTarget.showLimitSettings = EditorGUILayout.BeginToggleGroup ("Show Camera Limit Settings!", myTarget.showLimitSettings);

			if (myTarget.showLimitSettings) {				
				GUILayout.BeginVertical ("", boxStyle);
				GUILayout.Space (15);

				myTarget.CamHeight = EditorGUILayout.Slider ("Height", myTarget.CamHeight, 0f, 25f);
				myTarget.CamSpeed = EditorGUILayout.Slider ("Speed", myTarget.CamSpeed, 0.1f, 2f);
				myTarget.CamHeightSpeedMulti = EditorGUILayout.Slider ("Height/Speed Multiplier", myTarget.CamHeightSpeedMulti, 0.1f, 10f);

				myTarget.CamZoomMin = EditorGUILayout.Slider ("Zoom Min", myTarget.CamZoomMin, 0f, 25f);
				myTarget.CamZoomMax = EditorGUILayout.Slider ("Zoom Max", myTarget.CamZoomMax, 0f, 25f);

				myTarget.CamFOVMin = EditorGUILayout.Slider ("FOV Min", myTarget.CamFOVMin, 1f, 120f);
				myTarget.CamFOVMax = EditorGUILayout.Slider ("FOV Max", myTarget.CamFOVMax, 1f, 120f);

				myTarget.CamVerticalMin = EditorGUILayout.Slider ("Vertical Min", myTarget.CamVerticalMin, -100f, 100f);
				myTarget.CamVerticalMax = EditorGUILayout.Slider ("Vertical Max", myTarget.CamVerticalMax, -100f, 100f);

				myTarget.CamHorizontalMin = EditorGUILayout.Slider ("Horizontal Min", myTarget.CamHorizontalMin, -100f, 100f);
				myTarget.CamHorizontalMax = EditorGUILayout.Slider ("Horizontal Max", myTarget.CamHorizontalMax, -100f, 100f);

				GUILayout.Space (15);	
				EditorGUILayout.EndVertical ();
			}

			EditorGUILayout.EndToggleGroup ();
			EditorGUILayout.EndVertical ();
			//

			serializedObj.ApplyModifiedProperties ();

			// END
			EditorGUILayout.EndVertical ();
			EditorUtility.SetDirty (target);
		}
	}
}

