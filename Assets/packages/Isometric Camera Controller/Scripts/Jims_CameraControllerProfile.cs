﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Jims.InputSystem;


namespace Jims.Profile
{	
	[CreateAssetMenu (fileName = "Camera Profile", menuName = "Camera Profile", order = 150)]
	public partial class Jims_CameraControllerProfile : ScriptableObject
	{		
		#region

		//USED WITH EDITOR!
		public bool showInputSettings = false;
		public bool showLimitSettings = false;
		public bool showHelp = true;

		#endregion

		#region

		[Header ("Inputs")]
		[SerializeField]
		public InputType InputSystem;
		public enum  InputType
		{
			Windows,
			Mobile}
		;

		#endregion

		#region

		[SerializeField]
		public MovementMode MoveMode;
		public enum  MovementMode
		{
			Keyboard,
			ClickDrag,
			both}
		;

		#endregion

		#region

		[SerializeField]
		[Tooltip ("Mouse Scroll Wheel Input Name!")]
		public string MouseScrollWheelInputName = "Mouse ScrollWheel";
		[SerializeField]
		[Tooltip ("Zoom In Input Key!")]
		public KeyCode ZoomInKey;
		[SerializeField]
		[Tooltip ("Zoom Out Input Key!")]
		public KeyCode ZoomOutKey;
		[SerializeField]
		[Tooltip ("Drag Camera Key!")]
		public KeyCode CameraDragKey;
		[SerializeField]
		[Tooltip ("Reset Camera Key!")]
		public KeyCode CameraResetKey;

		#endregion

		#region

		[Header ("Settings")]
		[SerializeField]
		[Range (0f, 25f)]
		[Tooltip ("Set Camera default height!")]
		protected float m_CamHeight = 8f;
		public float CamHeight {
			get { return m_CamHeight; }
			set { m_CamHeight = value; }
		}

		[SerializeField]
		[Range (0f, 25f)]
		[Tooltip ("If Camera is orthographic, set size value!")]
		protected float m_CamZoomMin = 6f;
		public float CamZoomMin {
			get { return m_CamZoomMin; }
			set { m_CamZoomMin = value; }
		}

		[SerializeField]
		[Range (0f, 25f)]
		[Tooltip ("If Camera is orthographic, set size value!")]
		protected float m_CamZoomMax = 10f;
		public float CamZoomMax {
			get { return m_CamZoomMax; }
			set { m_CamZoomMax = value; }
		}

		[SerializeField]
		[Range (1f, 120f)]
		[Tooltip ("If Camera is NOT orthographic, set FOV value!")]
		protected float m_CamFOVMin = 40f;
		public float CamFOVMin {
			get { return m_CamFOVMin; }
			set { m_CamFOVMin = value; }
		}

		[SerializeField]
		[Range (1f, 120f)]
		[Tooltip ("If Camera is NOT orthographic, set FOV value!")]
		protected float m_CamFOVMax = 60f;
		public float CamFOVMax {
			get { return m_CamFOVMax; }
			set { m_CamFOVMax = value; }
		}

		[SerializeField]
		[Range (-100f, 100f)]
		[Tooltip ("Set backwards limit!")]
		protected float m_CamVerticalMin = -20f;
		public float CamVerticalMin {
			get { return m_CamVerticalMin; }
			set { m_CamVerticalMin = value; }
		}

		[SerializeField]
		[Range (-100f, 100f)]
		[Tooltip ("Set forwards limit!")]
		protected float m_CamVerticalMax = 65f;
		public float CamVerticalMax {
			get { return m_CamVerticalMax; }
			set { m_CamVerticalMax = value; }
		}

		[SerializeField]
		[Range (-100f, 100f)]
		[Tooltip ("Set left limit!")]
		protected float m_CamHorizontalMin = -20f;
		public float CamHorizontalMin {
			get { return m_CamHorizontalMin; }
			set { m_CamHorizontalMin = value; }
		}

		[SerializeField]
		[Range (-100f, 100f)]
		[Tooltip ("Set right limit!")]
		protected float m_CamHorizontalMax = 15f;
		public float CamHorizontalMax {
			get { return m_CamHorizontalMax; }
			set { m_CamHorizontalMax = value; }
		}

		[SerializeField]
		[Range (0.1f, 2f)]
		[Tooltip ("Set camera move speed multi!")]
		protected float m_CamSpeed = 0.3f;
		public float CamSpeed {
			get { return m_CamSpeed; }
			set { m_CamSpeed = value; }
		}

		[SerializeField]
		[Range (0.1f, 10f)]
		[Tooltip ("Divide by Multi, High is slower speed!")]
		protected float m_CamHeightSpeedMulti = 2f;
		public float CamHeightSpeedMulti {
			get { return m_CamHeightSpeedMulti; }
			set { m_CamHeightSpeedMulti = value; }
		}

		#endregion



		/// <summary>
		/// Apply Profile Settings to Camera!
		/// </summary>
		public void Load (Jims_CameraController Cam)
		{
			switch (InputSystem) {
			case InputType.Windows:
				{
					Cam.InputSystem = Jims_CameraController.InputType.Windows;
				}
				break;
			case InputType.Mobile:
				{
					Cam.InputSystem = Jims_CameraController.InputType.Mobile;
				}
				break;
			}

			switch (MoveMode) {
			case MovementMode.Keyboard:
				{
					Cam.MoveMode = Jims_CameraController.MovementMode.Keyboard;
				}
				break;
			case MovementMode.ClickDrag:
				{
					Cam.MoveMode = Jims_CameraController.MovementMode.ClickDrag;
				}
				break;
			case MovementMode.both:
				{
					Cam.MoveMode = Jims_CameraController.MovementMode.both;
				}
				break;
			}

			Cam.MouseScrollWheelInputName = MouseScrollWheelInputName;
			Cam.ZoomInKey = ZoomInKey;
			Cam.ZoomOutKey = ZoomOutKey;
			Cam.CameraDragKey = CameraDragKey;
			Cam.CameraResetKey = CameraResetKey;

			Cam.CamHeight = CamHeight;
			Cam.CamSpeed = CamSpeed;
			Cam.CamHeightSpeedMulti = CamHeightSpeedMulti;

			Cam.CamZoomMin = CamZoomMin;
			Cam.CamZoomMax = CamZoomMax;
			Cam.CamFOVMin = CamFOVMin;
			Cam.CamFOVMax = CamFOVMax;
			Cam.CamVerticalMin = CamVerticalMin;
			Cam.CamVerticalMax = CamVerticalMax;
			Cam.CamHorizontalMin = CamHorizontalMin;
			Cam.CamHorizontalMax = CamHorizontalMax;
		}

		/// <summary>
		/// Replace Profile Settings with Camera Settings
		/// </summary>
		public void Save (Jims_CameraController Cam)
		{
			switch (Cam.InputSystem) {
			case Jims_CameraController.InputType.Windows:
				{
					InputSystem = Jims_CameraControllerProfile.InputType.Windows;
				}
				break;
			case Jims_CameraController.InputType.Mobile:
				{
					InputSystem = Jims_CameraControllerProfile.InputType.Mobile;
				}
				break;
			}

			switch (Cam.MoveMode) {
			case Jims_CameraController.MovementMode.Keyboard:
				{
					MoveMode = Jims_CameraControllerProfile.MovementMode.Keyboard;
				}
				break;
			case Jims_CameraController.MovementMode.ClickDrag:
				{
					MoveMode = Jims_CameraControllerProfile.MovementMode.ClickDrag;
				}
				break;
			case Jims_CameraController.MovementMode.both:
				{
					MoveMode = Jims_CameraControllerProfile.MovementMode.both;
				}
				break;
			}

			MouseScrollWheelInputName = Cam.MouseScrollWheelInputName;
			ZoomInKey = Cam.ZoomInKey;
			ZoomOutKey = Cam.ZoomOutKey;
			CameraDragKey = Cam.CameraDragKey;
			CameraResetKey = Cam.CameraResetKey;

			CamHeight = Cam.CamHeight;
			CamSpeed = Cam.CamSpeed;
			CamHeightSpeedMulti = Cam.CamHeightSpeedMulti;

			CamZoomMin = Cam.CamZoomMin;
			CamZoomMax = Cam.CamZoomMax;
			CamFOVMin = Cam.CamFOVMin;
			CamFOVMax = Cam.CamFOVMax;
			CamVerticalMin = Cam.CamVerticalMin;
			CamVerticalMax = Cam.CamVerticalMax;
			CamHorizontalMin = Cam.CamHorizontalMin;
			CamHorizontalMax = Cam.CamHorizontalMax;
		}
	}
}
