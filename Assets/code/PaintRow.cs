// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// PaintRow
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PaintRow : MonoBehaviour
{
	public static PaintRow pr;

	public TextMeshProUGUI txt;

	private PaintInfo info;

	public Button butPrefab;

	public Image artist;

	public Image style;

	public Image subject;

	public Transform detBar;

	public int lenArtist = 14;

	public int lenSubject = 14;

	public int lenStyle = 14;

	public int lenScore = 10;

	public float rowHeight = 30f;

	private void Start()
	{
		pr = this;
	}

	public void SetUp(PaintInfo info)
	{
		//IL_000e: Unknown result type (might be due to invalid IL or missing references)
		//IL_001f: Unknown result type (might be due to invalid IL or missing references)
		//IL_0030: Unknown result type (might be due to invalid IL or missing references)
		this.info = info;
		artist.color =info.cArtist;
		style.color =info.cStyle;
		subject.color =info.cSubject;
		txt.text =info.score.ToString().PadRight(lenScore) + " " + info.nArtist.PadRight(lenArtist) + " " + info.nStyle.PadRight(lenStyle) + " " + info.nSubject.PadRight(lenSubject);
	}

	public void OnGetClick()
	{
		GameController.gc.MovePainting(info);
	}

	private void Update()
	{
		//IL_0017: Unknown result type (might be due to invalid IL or missing references)
		//IL_001c: Unknown result type (might be due to invalid IL or missing references)
		//IL_003c: Unknown result type (might be due to invalid IL or missing references)
		if (info != null)
		{
			Vector3 localScale = detBar.localScale;
			detBar.localScale =new Vector3(info.currentPercent, localScale.y, localScale.x);
		}
	}
}
