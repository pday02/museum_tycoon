// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// UINumbers
using UnityEngine;
using UnityEngine.UI;

public class UINumbers : MonoBehaviour
{
	public Text moneyAmount;
    public Text timeText;
    public TimeManager tm;

	private void Start()
	{
        tm.MinuteChanged += OnMinuteChanged;
        OnMinuteChanged(null, tm.GetTime());
	}

	private void Update()
	{
		if (InventoryManager.im != null)
		{
			moneyAmount.text =InventoryManager.im.money.ToString();
		}
	}

    private void OnMinuteChanged(object sender, GameTime e)
    {
        timeText.text = e.Hour.ToString().PadLeft(2,'0') + ":" + e.Minute.ToString().PadLeft(2,'0');
    }
}
