// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// RoomTileScript
using UnityEngine;

public class RoomTileScript : MonoBehaviour
{
	public int x;

	public int y;

	public float height;

	public MeshRenderer mesh;

	private FloorPlan fp;

	private void Start()
	{
	}

	private void Update()
	{
	}

	public void SetUp(int x, int y, FloorPlan fp)
	{
		//IL_0016: Unknown result type (might be due to invalid IL or missing references)
		//IL_001b: Unknown result type (might be due to invalid IL or missing references)
		//IL_0036: Unknown result type (might be due to invalid IL or missing references)
		this.x = x;
		this.y = y;
		this.fp = fp;
		this.gameObject.transform.position = new Vector3((float)x + 0.5f, height, (float)y + 0.5f);
		fp.RegTile(this);
	}

	public void SetColour(Color c)
	{
		//IL_0006: Unknown result type (might be due to invalid IL or missing references)
		//IL_000b: Unknown result type (might be due to invalid IL or missing references)
		mesh.material.color =c;
	}
}
