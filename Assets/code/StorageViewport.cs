﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StorageViewport : MonoBehaviour
{
    public Button prefab;

    public Transform contentPanel;

    public RectTransform contentRect;

    public PaintRow prPrefab;

    private List<PaintRow> rows;

    private Vector3 startingPosition = new Vector3(-360f, -323f, 0f);

    private int placed;

    private Vector2 startingSize;


    private void Start()
    {
        //IL_0012: Unknown result type (might be due to invalid IL or missing references)
        //IL_0017: Unknown result type (might be due to invalid IL or missing references)
        rows = new List<PaintRow>();
        startingSize = contentRect.sizeDelta;
        InventoryManager.im.PaintStorageChanged += C_StorageChange;
        C_StorageChange(null, null);
    }

    private void Update()
    {
    }

    public void AddPainting(PaintInfo pi)
    {
        //IL_000d: Unknown result type (might be due to invalid IL or missing references)
        //IL_0025: Unknown result type (might be due to invalid IL or missing references)
        //IL_002b: Unknown result type (might be due to invalid IL or missing references)
        //IL_0030: Unknown result type (might be due to invalid IL or missing references)
        //IL_003b: Unknown result type (might be due to invalid IL or missing references)
        //IL_0047: Unknown result type (might be due to invalid IL or missing references)
        //IL_004c: Unknown result type (might be due to invalid IL or missing references)
        //IL_006b: Unknown result type (might be due to invalid IL or missing references)
        //IL_007b: Unknown result type (might be due to invalid IL or missing references)
        //IL_0080: Unknown result type (might be due to invalid IL or missing references)
        PaintRow paintRow = Instantiate(prPrefab);
        paintRow.transform.SetParent(contentPanel);
        paintRow.SetUp(pi);
        paintRow.transform.localPosition = (startingPosition - Vector3.up * paintRow.rowHeight * (float)placed);
        placed++;
        RectTransform obj = contentRect;
        obj.sizeDelta = (obj.sizeDelta + new Vector2(0f, paintRow.rowHeight));
        rows.Add(paintRow);
    }

    private void Reset()
    {
        //IL_001a: Unknown result type (might be due to invalid IL or missing references)
        //IL_004a: Unknown result type (might be due to invalid IL or missing references)
        foreach (PaintRow row in rows)
        {
            Destroy(row.gameObject);
        }
        contentRect.sizeDelta = (startingSize);
        placed = 0;
        rows = new List<PaintRow>();
    }

    private void C_StorageChange(object sender, EventArgs e)
    {
        Reset();
        InventoryManager.im.storedPaintings.ForEach(AddPainting);
    }
}
