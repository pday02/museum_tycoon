// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// DoorScript
using UnityEngine;

public class DoorScript : MonoBehaviour 
{
	private WallScript[] walls;

	private Transform trans;

	private GameController gc;


	private void Start()
	{
		trans = this.GetComponent<Transform>();
	}

	private void Update()
	{
	}

	public void OnDestroy()
	{
        
		if (walls[0] != null)walls[0].RemoveDoor();
		if (walls[1] != null) walls[1].RemoveDoor();
		Object.Destroy(this.gameObject);
	}

	public void SetUp(WallScript[] walls, GameController gc)
	{

		this.gc = gc;
		this.walls = walls;
		walls[0].ReplaceWithDoor(this);
		walls[1].ReplaceWithDoor(this);
		Vector3 val = (walls[0].dir != 0) ? new Vector3(0f, 0f, 0.5f) : new Vector3(0.5f, 0f);
		this.transform.position = walls[0].transform.position + val;
		float num = (walls[0].dir != Direction.V) ? 90f : 0f;
		Vector3 val2 = new Vector3(0.0f,num);
		this.transform.Rotate(val2);
		DoorDestroyButton componentInChildren = this.GetComponentInChildren<DoorDestroyButton>();
		componentInChildren.SetUp(gc, this);
	}
}
