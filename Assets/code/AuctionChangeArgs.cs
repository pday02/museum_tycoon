// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// AuctionChangeArgs
using System;

public class AuctionChangeArgs : EventArgs
{
	public bool NewAuction
	{
		get;
		set;
	}

	public bool SellingPlayer
	{
		get;
		set;
	}

	public bool PlayerWinning
	{
		get;
		set;
	}

	public PaintInfo B1
	{
		get;
		set;
	}

	public PaintInfo B2
	{
		get;
		set;
	}

	public PaintInfo B3
	{
		get;
		set;
	}

	public PaintInfo BL
	{
		get;
		set;
	}

	public PaintInfo S1
	{
		get;
		set;
	}

	public PaintInfo S2
	{
		get;
		set;
	}

	public PaintInfo S3
	{
		get;
		set;
	}

	public PaintInfo SL
	{
		get;
		set;
	}

	public int TimeLeft
	{
		get;
		set;
	}

	public int NextBidAmount
	{
		get;
		set;
	}

	public int LastSell
	{
		get;
		set;
	}

	public int LastBuy
	{
		get;
		set;
	}

	public int ThisSell
	{
		get;
		set;
	}

	public int ThisBuy
	{
		get;
		set;
	}
}
