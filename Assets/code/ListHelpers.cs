// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// ListHelpers
using System;
using System.Collections.Generic;
using System.Linq;

public class ListHelpers
{
	public static List<T> Clone<T>(List<T> x) where T : ICloneable
	{
		return (from i in x
		select (T)i.Clone()).ToList();
	}

	public static T Median<T>(List<T> list) where T : IComparable
	{
		int index = list.Count() / 2;
		IOrderedEnumerable<T> source = from n in list
		orderby n
		select n;
		return source.ElementAt(index);
	}
}
