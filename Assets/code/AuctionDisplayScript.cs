// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// AuctionDisplayScript
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AuctionDisplayScript : MonoBehaviour
{
	public AuctionItemDisp s1;

	public AuctionItemDisp s2;

	public AuctionItemDisp s3;

	public AuctionItemDisp b1;

	public AuctionItemDisp b2;

	public AuctionItemDisp b3;

	public AuctionItemDisp sl;

	public AuctionItemDisp bl;

	public TextMeshProUGUI txtTimeLeft;

	public TextMeshProUGUI txtLastBuy;

	public TextMeshProUGUI txtLastSell;

	public TextMeshProUGUI txtBuyBid;

	public TextMeshProUGUI txtSellBid;

	public Text txtBidButton;

	public AuctionManager am;

	public GameController gc;

	public Image sellBackground;

	public Image buyBackground;

	public TextMeshProUGUI autoBidText;

	private Color sellBackgroundColour;

	private Color buyBackgroundColour;

	public Color highlightBackgroundColour;

	public Image winningIndicator;

	public Color winColour;

	public Color loseColour;

	private Color noWLColour;

	private PaintInfo pi;

	private GameState currentState;

	private void Start()
	{
		//IL_0035: Unknown result type (might be due to invalid IL or missing references)
		//IL_003a: Unknown result type (might be due to invalid IL or missing references)
		//IL_0046: Unknown result type (might be due to invalid IL or missing references)
		//IL_004b: Unknown result type (might be due to invalid IL or missing references)
		am.AuctionChanged += OnAuctionChange;
		gc.GameStateChanged += OnStateChange;
		sellBackgroundColour = sellBackground.color;
		buyBackgroundColour = buyBackground.color;
	}

	public void BidButtonClick()
	{
		am.MakePlayerBid();
	}

	public void SellNextClicked()
	{
		if (currentState == GameState.PlacePainting && pi != null)
		{
			am.AddNextPaintingToSell(pi);
			gc.SetGameState(GameState.None);
		}
		else if (s2 != null)
		{
			am.RemoveNextPaintingToSell();
		}
	}

	public void Sell2ndClicked()
	{
		if (currentState == GameState.PlacePainting && pi != null)
		{
			am.Add2ndPaintingToSell(pi);
			gc.SetGameState(GameState.None);
		}
		else if (s3 != null)
		{
			am.Remove2ndPaintingToSell();
		}
	}

	private void OnStateChange(object sender, GameStateChangeArgs e)
	{
		pi = e.Painting;
		currentState = e.Next;
	}

	private void OnAuctionChange(object sender, AuctionChangeArgs e)
	{
		//IL_0180: Unknown result type (might be due to invalid IL or missing references)
		//IL_0191: Unknown result type (might be due to invalid IL or missing references)
		//IL_01a2: Unknown result type (might be due to invalid IL or missing references)
		//IL_01b8: Unknown result type (might be due to invalid IL or missing references)
		//IL_01c9: Unknown result type (might be due to invalid IL or missing references)
		//IL_01e5: Unknown result type (might be due to invalid IL or missing references)
		//IL_01f0: Unknown result type (might be due to invalid IL or missing references)
		if (e.NewAuction)
		{
			autoBidText.text ="0";
		}
		s1.DisplayPainting(e.S1);
		s2.DisplayPainting(e.S2);
		s3.DisplayPainting(e.S3);
		sl.DisplayPainting(e.SL);
		b1.DisplayPainting(e.B1);
		b2.DisplayPainting(e.B2);
		b3.DisplayPainting(e.B3);
		bl.DisplayPainting(e.BL);
		txtTimeLeft.text =e.TimeLeft.ToString();
		txtLastBuy.text =e.LastBuy.ToString();
		txtLastSell.text =e.LastSell.ToString();
		txtBuyBid.text =e.ThisBuy.ToString();
		txtSellBid.text =e.ThisSell.ToString();
		txtBidButton.text ="Bid (" + e.NextBidAmount.ToString() + ")";
		if (e.SellingPlayer)
		{
			sellBackground.color =highlightBackgroundColour;
			buyBackground.color =buyBackgroundColour;
			winningIndicator.color =noWLColour;
		}
		else
		{
			sellBackground.color =sellBackgroundColour;
			buyBackground.color =highlightBackgroundColour;
			winningIndicator.color =(!e.PlayerWinning) ? loseColour : winColour;
		}
		int result = 0;
		string value = Regex.Match(autoBidText.text, "\\d+").Value;
		if (int.TryParse(value, out result) && !e.PlayerWinning && result >= e.NextBidAmount && !e.SellingPlayer)
		{
			am.MakePlayerBid();
		}
	}
}
