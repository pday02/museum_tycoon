﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TestText : MonoBehaviour {

    TextMeshPro txt;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        txt = this.GetComponent<TextMeshPro>();
        txt.text = "One\nTwo";
	}
}
