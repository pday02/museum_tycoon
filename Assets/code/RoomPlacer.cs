// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// RoomPlacer
using UnityEngine;

public class RoomPlacer : MonoBehaviour
{
	public WallScript wall;
	public MainDoorScript mainDoor;
	public WallCreateButton createWall;
	public RoomTileScript tile;
    public VisitorEnterManager visEnterMan;


	private FloorPlan floorPlan;

	private GlobalSettings settings;



	private void Start()
	{
		settings = this.gameObject.GetComponent<GlobalSettings>();
		int num = settings.environ_size[0] / 2 - 1;
		int num2 = num + 2;
		floorPlan = new FloorPlan(settings.environ_size[0], settings.environ_size[1]);
		RoomManager.rm.SetFloorPlan(floorPlan);
		for (int i = 0; i < settings.environ_size[0]; i++)
		{
			CreateWall(i, settings.environ_size[1], Direction.H);
			if (i < num || i >= num2)
			{
				CreateWall(i, 0, Direction.H);
			}
		}
		for (int j = 0; j < settings.environ_size[1]; j++)
		{
			CreateWall(0, j, Direction.V);
			CreateWall(settings.environ_size[0], j, Direction.V);
		}
		MainDoorScript mainDoorScript = Instantiate(mainDoor);
		mainDoorScript.PlaceAt(num,visEnterMan);
		for (int k = 1; k < settings.environ_size[0]; k++)
		{
			for (int l = 0; l < settings.environ_size[1]; l++)
			{
				CreateWallButton(k, l, Direction.V);
			}
		}
		for (int m = 0; m < settings.environ_size[0]; m++)
		{
			for (int n = 1; n < settings.environ_size[1]; n++)
			{
				CreateWallButton(m, n, Direction.H);
			}
		}
		for (int num3 = 0; num3 < settings.environ_size[0]; num3++)
		{
			for (int num4 = 0; num4 < settings.environ_size[1]; num4++)
			{
				RoomTileScript roomTileScript = Instantiate(tile);
				roomTileScript.SetUp(num3, num4, floorPlan);
			}
		}
	}

	private void CreateWall(int x, int y, Direction d)
	{
		//IL_0024: Unknown result type (might be due to invalid IL or missing references)
		WallScript wallScript = Instantiate(wall);
		wallScript.SetUp(new int[2]
		{
			x,
			y
		}, d, true, floorPlan, this.gameObject.GetComponent<GameController>(), null);
	}

	private void CreateWallButton(int x, int y, Direction d)
	{
		//IL_0029: Unknown result type (might be due to invalid IL or missing references)
		WallCreateButton wallCreateButton = Instantiate(createWall);
		wallCreateButton.PlaceAt(new int[2]
		{
			x,
			y
		}, d);
		wallCreateButton.SetUp(wall, this.gameObject.GetComponent<GameController>(), floorPlan);
	}

	private void Update()
	{
	}
}
