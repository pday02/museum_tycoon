// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// RoomInfoDisplay
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class RoomInfoDisplay : MonoBehaviour
{
	private GameController gc;

	public TextMeshPro display;

	public void DisplayString(string str, RoomInfo room, GameController gc)
	{
		//IL_00de: Unknown result type (might be due to invalid IL or missing references)
		//IL_00e3: Unknown result type (might be due to invalid IL or missing references)
		//IL_00e8: Unknown result type (might be due to invalid IL or missing references)
		//IL_00ed: Unknown result type (might be due to invalid IL or missing references)
		//IL_00f9: Unknown result type (might be due to invalid IL or missing references)
		//IL_00fe: Unknown result type (might be due to invalid IL or missing references)
		//IL_010b: Unknown result type (might be due to invalid IL or missing references)
		this.gc = gc;
		int xMed = ListHelpers.Median((from t in room.tiles
		select t.x).ToList());
		List<RoomTileScript> list = (from t in room.tiles
		where t.x == xMed
		select t).ToList();
		RoomTileScript roomTileScript = list[0];
		if (list.Count() > 1)
		{
			int yMed = ListHelpers.Median((from t in list
			select t.y).ToList());
			roomTileScript = (from t in list
			where t.y == yMed
			select t).ToList().ElementAt(0);
		}
		int x = roomTileScript.x;
		int y = roomTileScript.y;
		Vector3 position = this.gameObject.transform.position ;
		float y2 = position.y;
		this.gameObject.transform.position = new Vector3((float)x, y2, (float)y);
		display.text =str;
		gc.GameStateChanged += On_GameStateChanged;
	}

	private void On_GameStateChanged(object sender, GameStateChangeArgs e)
	{
		//IL_0018: Unknown result type (might be due to invalid IL or missing references)
		gc.GameStateChanged -= On_GameStateChanged;
		Object.Destroy(this.gameObject);
	}

	private void Start()
	{
	}

	private void Update()
	{
	}
}
