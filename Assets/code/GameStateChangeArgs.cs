// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// GameStateChangeArgs
using System;

public class GameStateChangeArgs2 : EventArgs
{
	public GameState Previous
	{
		get;
		set;
	}

	public GameState Next
	{
		get;
		set;
	}

	public PaintInfo Painting
	{
		get;
		set;
	}
}
