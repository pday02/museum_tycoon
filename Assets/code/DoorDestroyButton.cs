// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// DoorDestroyButton
using UnityEngine;

public class DoorDestroyButton : MonoBehaviour
{
	private DoorScript ds;

	private GameController gc;

	private void Start()
	{
	}

	private void Update()
	{
	}

	public void OnMouseDown()
	{
		gc.GameStateChanged -= On_GameStateChanged;
		Object.Destroy(ds);
	}

	private void On_GameStateChanged(object sender, GameStateChangeArgs e)
	{
		//IL_000d: Unknown result type (might be due to invalid IL or missing references)
		//IL_001e: Unknown result type (might be due to invalid IL or missing references)
		if (e.Next == GameState.DestroyDoors)
		{
			this.gameObject.SetActive(true);
		}
		else
		{
			this.gameObject.SetActive(false);
		}
	}

	public void SetUp(GameController gc, DoorScript ds)
	{
		//IL_0026: Unknown result type (might be due to invalid IL or missing references)
		this.gc = gc;
		this.ds = ds;
		this.gc.GameStateChanged += On_GameStateChanged;
		this.gameObject.SetActive(false);
	}
}
