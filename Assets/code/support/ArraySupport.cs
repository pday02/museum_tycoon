﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ArraySupport {

    private static System.Random ran = new System.Random();

    public static T GetRandom<T>(T[] array)
    {
        return array[ran.Next(array.Length)];
    }
}
