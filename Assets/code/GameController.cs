﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState {None,PlaceWalls, PlaceDoors, DestroyDoors, DestroyWalls, PlacePainting, ShowRooms}

public class GameController : MonoBehaviour {

    public static GameController gc;

    public KeyCode placeWalls;
    public KeyCode placeDoors;
    public KeyCode destroyDoors;
    public KeyCode destroyWalls;
    public KeyCode placePainting;
    public KeyCode showRooms;

    private GameState CurrentState = GameState.None; 


	// Use this for initialization
	void Awake () {
        gc = this;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(placeWalls))
        {
            OnGameStateChanged(GameState.PlaceWalls,null);
        } else if (Input.GetKeyDown(placeDoors)) {
            OnGameStateChanged(GameState.PlaceDoors,null);
        } else if (Input.GetKeyDown(destroyDoors))
        {
            OnGameStateChanged(GameState.DestroyDoors,null);
        } else if (Input.GetKeyDown(destroyWalls))
        {
            OnGameStateChanged(GameState.DestroyWalls,null);
        } else if (Input.GetKeyDown(placePainting))
        {
            OnGameStateChanged(GameState.PlacePainting,null);
        } else if (Input.GetKeyDown(showRooms))
        {
            OnGameStateChanged(GameState.ShowRooms, null);
        }

    }

    public void SetGameState(GameState gs)
    {
        OnGameStateChanged(gs,null);
    }

    public GameState GetGameState()
    {
        return gc.CurrentState;
    }

    public void MovePainting(PaintInfo p)
    {
        OnGameStateChanged(GameState.PlacePainting, p);
    }

    protected virtual void OnGameStateChanged(GameState newGS,PaintInfo p)
    {
        if (CurrentState == newGS)
        {
            CurrentState = GameState.None;
            var e = new GameStateChangeArgs
            {
                Next = GameState.None,
                Previous = newGS,
                Painting = p
            };
            GameStateChanged?.Invoke(this, e);
        }
        else
        {
            var e = new GameStateChangeArgs
            {
                Next = newGS,
                Previous = CurrentState,
                Painting = p
            };
            CurrentState = newGS;
            GameStateChanged?.Invoke(this, e);
        }
    }
    

    public event EventHandler<GameStateChangeArgs> GameStateChanged;
}

public class GameStateChangeArgs : EventArgs
{
    public GameState Previous { get; set; }
    public GameState Next { get; set; }
    public PaintInfo Painting { get; set; }
}