// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// GameState
public enum GameState2
{
	None,
	PlaceWalls,
	PlaceDoors,
	DestroyDoors,
	DestroyWalls,
	PlacePainting,
	ShowRooms
}
