// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// FloorPlan
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class FloorPlan
{
	public WallScript[,] hWalls;

	public WallScript[,] vWalls;

	public RoomTileScript[,] tiles;

    public List<RoomInfo> rooms;

	public int x;
	public int y;

	public FloorPlan(int x, int y)
	{
		this.x = x;
		this.y = y;
		hWalls = new WallScript[x, y + 1];
		vWalls = new WallScript[x + 1, y];
		tiles = new RoomTileScript[x, y];
	}

    public void SetRooms(List<RoomInfo> r)
    {
        rooms = r;
    }

    public List<PaintInfo> GetPaintings()
    {
        if (rooms == null) return new List<PaintInfo>();
        return rooms.SelectMany(x => x.paintings).ToList();
    }

    public List<RoomInfo> GetRooms()
    {
        return rooms;
    }

	public void RegTile(RoomTileScript r)
	{
		tiles[r.x, r.y] = r;
	}

	public void RegWall(WallScript w)
	{
		if (w.dir == Direction.H)
		{
			hWalls[w.x, w.y] = w;
		}
		else
		{
			vWalls[w.x, w.y] = w;
		}
	}

	public void UnRegWall(WallScript w)
	{
		WallScript[,] array = (w.dir != 0) ? vWalls : hWalls;
		array[w.x, w.y] = null;
	}

	public bool ValidWallPlacement(int w_x, int w_y, Direction dir)
	{
		Direction dir2 = (dir == Direction.H) ? Direction.V : Direction.H;
		if (dir == Direction.H)
		{
			return (NotDoor(w_x, w_y, dir2) || NotDoor(w_x, w_y - 1, dir2)) && (NotDoor(w_x + 1, w_y - 1, dir2) || NotDoor(w_x + 1, w_y, dir2));
		}
		return (NotDoor(w_x, w_y, dir2) || NotDoor(w_x - 1, w_y, dir2)) && (NotDoor(w_x - 1, w_y + 1, dir2) || NotDoor(w_x, w_y + 1, dir2));
	}

	private bool NotDoor(int w_x, int w_y, Direction dir)
	{
		if (dir == Direction.H)
		{
			if (w_x < 0 || w_y < 0 || w_x >= x || w_y > y)
			{
				return true;
			}
			WallScript wallScript = hWalls[w_x, w_y];
			return wallScript == null || !wallScript.door;
		}
		if (w_x < 0 || w_y < 0 || w_x > x || w_y >= y)
		{
			return true;
		}
		WallScript wallScript2 = vWalls[w_x, w_y];
		return wallScript2 == null || !wallScript2.door;
	}

	public WallScript[] NextWallDoor(int w_x, int w_y, Direction dir)
	{
		int num = (dir != 0) ? x : (x - 2);
		int num2 = (dir != Direction.V) ? y : (y - 2);
		if (w_x < 0 || w_y < 0 || w_x > num || w_y > num2)
		{
			return null;
		}
		WallScript wallScript = (dir != 0) ? vWalls[w_x, w_y] : hWalls[w_x, w_y];
		if (wallScript == null)
		{
			return null;
		}
		if (!wallScript.CanBeDoor())
		{
			return null;
		}
		WallScript wallScript2 = (wallScript.dir != 0) ? vWalls[wallScript.x, wallScript.y + 1] : hWalls[wallScript.x + 1, wallScript.y];
		WallScript wallScript3 = (wallScript.dir != 0) ? hWalls[wallScript.x, wallScript.y + 1] : vWalls[wallScript.x + 1, wallScript.y];
		WallScript wallScript4 = (wallScript.dir != 0) ? hWalls[wallScript.x - 1, wallScript.y + 1] : vWalls[wallScript.x + 1, wallScript.y - 1];
		if (wallScript2 != null && wallScript2.CanBeDoor() && wallScript3 == null && wallScript4 == null)
		{
			return new WallScript[2]
			{
				wallScript,
				wallScript2
			};
		}
		return null;
	}

	public bool IsTileIn(Vector2Int v)
	{
		return v.x >= 0 && v.x < x && v.y >= 0 && v.y < y;
	}

	public WallScript GetWall(int x, int y, Direction dir)
	{
		if (dir == Direction.H)
		{
			return hWalls[x, y];
		}
		return vWalls[x, y];
	}

	public List<PaintInfo> GetPaintingsAtTile(int x, int y)
	{
		List<PaintInfo> list = new List<PaintInfo>();
		if (hWalls[x, y] != null && hWalls[x, y].pBack != null)
		{
			list.Add(hWalls[x, y].pBack);
		}
		if (hWalls[x, y + 1] != null && hWalls[x, y + 1].pFront != null)
		{
			list.Add(hWalls[x, y + 1].pFront);
		}
		if (vWalls[x, y] != null && vWalls[x, y].pFront != null)
		{
			list.Add(vWalls[x, y].pFront);
		}
		if (vWalls[x + 1, y] != null && vWalls[x + 1, y].pBack != null)
		{
			list.Add(vWalls[x + 1, y].pBack);
		}
		return list;
	}

	public List<Vector2Int> GetTilesThroughDoor(int x, int y)
	{

		List<Vector2Int> list = new List<Vector2Int>();
		if (hWalls[x, y] != null && hWalls[x, y].door != null && IsTileIn(new Vector2Int(x, y - 1)))
		{
			list.Add(new Vector2Int(x, y - 1));
		}
		if (hWalls[x, y + 1] != null && hWalls[x, y + 1].door != null && IsTileIn(new Vector2Int(x, y + 1)))
		{
			list.Add(new Vector2Int(x, y + 1));
		}
		if (vWalls[x, y] != null && vWalls[x, y].door != null && IsTileIn(new Vector2Int(x - 1, y)))
		{
			list.Add(new Vector2Int(x - 1, y));
		}
		if (vWalls[x + 1, y] != null && vWalls[x + 1, y].door != null && IsTileIn(new Vector2Int(x + 1, y)))
		{
			list.Add(new Vector2Int(x + 1, y));
		}
		return list;
	}

	public List<Vector2Int> GetConnectingTiles(int x, int y)
	{
		//IL_000b: Unknown result type (might be due to invalid IL or missing references)
		//IL_0037: Unknown result type (might be due to invalid IL or missing references)
		//IL_0046: Unknown result type (might be due to invalid IL or missing references)
		//IL_0074: Unknown result type (might be due to invalid IL or missing references)
		//IL_0083: Unknown result type (might be due to invalid IL or missing references)
		//IL_00b1: Unknown result type (might be due to invalid IL or missing references)
		//IL_00c0: Unknown result type (might be due to invalid IL or missing references)
		//IL_00ec: Unknown result type (might be due to invalid IL or missing references)
		List<Vector2Int> list = new List<Vector2Int>();
		if (IsTileIn(new Vector2Int(x, y - 1)) && hWalls[x, y] == null)
		{
			list.Add(new Vector2Int(x, y - 1));
		}
		if (IsTileIn(new Vector2Int(x, y + 1)) && hWalls[x, y + 1] == null)
		{
			list.Add(new Vector2Int(x, y + 1));
		}
		if (IsTileIn(new Vector2Int(x + 1, y)) && vWalls[x + 1, y] == null)
		{
			list.Add(new Vector2Int(x + 1, y));
		}
		if (IsTileIn(new Vector2Int(x - 1, y)) && vWalls[x, y] == null)
		{
			list.Add(new Vector2Int(x - 1, y));
		}
		return list;
	}
}
