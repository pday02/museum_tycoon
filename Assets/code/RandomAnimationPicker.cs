// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// RandomAnimationPicker
using UnityEngine;

public class RandomAnimationPicker : MonoBehaviour
{
	private Animator anim;

	private int AnimChoose;

	private float speed;

	private void Start()
	{
		anim = this.GetComponent<Animator>();
		anim.speed = 1f * Random.Range(0.5f, 1.5f);
		AnimChoose = Random.Range(1, 3);
		anim.SetInteger("Idle", AnimChoose);
	}
}
