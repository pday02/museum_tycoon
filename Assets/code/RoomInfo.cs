// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// RoomInfo
using System.Collections.Generic;

public class RoomInfo
{
	public List<PaintInfo> paintings;

	public List<RoomTileScript> tiles;

	public int roomID;

	public RoomInfo()
	{
		paintings = new List<PaintInfo>();
		tiles = new List<RoomTileScript>();
	}
}
