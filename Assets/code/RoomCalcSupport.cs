// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// RoomCalcSupport
using System.Collections.Generic;
using UnityEngine;

public class RoomCalcSupport
{
	public bool[,] searched;

	public List<Vector2Int> toSearch;

	public List<Vector2Int> otherRooms;

	public List<RoomInfo> otherRoomCalcs;

	public RoomInfo thisRoomInfo;

	public FloorPlan fp;

	public RoomCalcSupport(Vector2Int start, FloorPlan fp)
	{
		//IL_0056: Unknown result type (might be due to invalid IL or missing references)
		this.fp = fp;
		searched = new bool[fp.x, fp.y];
		toSearch = new List<Vector2Int>();
		otherRooms = new List<Vector2Int>();
		otherRoomCalcs = new List<RoomInfo>();
		thisRoomInfo = new RoomInfo();
		toSearch.Add(start);
		for (int i = 0; i < searched.GetLength(0); i++)
		{
			for (int j = 0; j < searched.GetLength(1); j++)
			{
				searched[i, j] = false;
			}
		}
	}
}
