﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SpeedDropdown : MonoBehaviour {

    public TMP_Dropdown dd;
    public TimeManager tm;

    public void SetSpeedInput(int i)
    {
        var n = int.Parse(dd.options[dd.value].text.Remove(0, 1));
        //tm.SetSpeed(n);
        Time.timeScale = n;
    }


}
