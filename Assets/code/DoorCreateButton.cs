// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// DoorCreateButton
using UnityEngine;

public class DoorCreateButton : MonoBehaviour
{
	private Transform trans;

	private int x;

	private int y;

	private Direction dir;

	public DoorScript doorScript;

	private FloorPlan floorPlan;

	private GameController gc;

	private WallScript[] walls;

	private void Start()
	{
		trans = this.GetComponent<Transform>();
	}

	private void Update()
	{
	}

	public void OnMouseDown()
	{
		DoorScript doorScript = Instantiate(this.doorScript);
		doorScript.SetUp(walls, gc);
		gc.SetGameState(GameState.None);
		gc.SetGameState(GameState.PlaceDoors);
	}

	public void OnDestroy()
	{
		gc.GameStateChanged -= On_GameStateChanged;
	}

	private void On_GameStateChanged(object sender, GameStateChangeArgs e)
	{
		//IL_0030: Unknown result type (might be due to invalid IL or missing references)
		//IL_004c: Unknown result type (might be due to invalid IL or missing references)
		if (e.Next == GameState.PlaceDoors)
		{
			walls = floorPlan.NextWallDoor(x, y, dir);
			this.gameObject.SetActive(walls != null);
		}
		else
		{
			this.gameObject.SetActive(false);
		}
	}

	public void SetUp(GameController gc, FloorPlan fp, int x, int y, Direction d)
	{
		//IL_003d: Unknown result type (might be due to invalid IL or missing references)
		this.gc = gc;
		floorPlan = fp;
		this.x = x;
		this.y = y;
		dir = d;
		this.gc.GameStateChanged += On_GameStateChanged;
		this.gameObject.SetActive(false);
	}
}
