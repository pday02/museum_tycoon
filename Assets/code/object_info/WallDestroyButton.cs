// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// WallDestroyButton
using UnityEngine;

public class WallDestroyButton : MonoBehaviour
{
	private WallScript ws;

	private GameController gc;

	public void OnMouseDown()
	{
		//IL_001d: Unknown result type (might be due to invalid IL or missing references)
		gc.GameStateChanged -= On_GameStateChanged;
		Object.Destroy(ws.gameObject);
	}

	private void On_GameStateChanged(object sender, GameStateChangeArgs e)
	{
		//IL_000d: Unknown result type (might be due to invalid IL or missing references)
		//IL_001e: Unknown result type (might be due to invalid IL or missing references)
		if (e.Next == GameState.DestroyWalls)
		{
			this.gameObject.SetActive(true);
		}
		else
		{
			this.gameObject.SetActive(false);
		}
	}

	public void SetUp(GameController gc, WallScript ds)
	{
		//IL_0026: Unknown result type (might be due to invalid IL or missing references)
		this.gc = gc;
		ws = ds;
		this.gc.GameStateChanged += On_GameStateChanged;
		this.gameObject.SetActive(false);
	}
}
