// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// PaintingPlaceButton
using UnityEngine;

public class PaintingPlaceButton : MonoBehaviour
{
	private WallScript ws;

	private WallSide side;

	private GameController gc;

	private int x;

	private int y;

	private Direction wallDir;

	public PaintInfo painting;

	private PaintInfo movePainting;

	public bool emptyClickCreatesNew;

	public void OnMouseDown()
	{
		//IL_0029: Unknown result type (might be due to invalid IL or missing references)
		//IL_002e: Unknown result type (might be due to invalid IL or missing references)
		//IL_0034: Unknown result type (might be due to invalid IL or missing references)
		//IL_0039: Unknown result type (might be due to invalid IL or missing references)
		//IL_003e: Unknown result type (might be due to invalid IL or missing references)
		//IL_00ae: Unknown result type (might be due to invalid IL or missing references)
		//IL_00c7: Unknown result type (might be due to invalid IL or missing references)
		//IL_00cc: Unknown result type (might be due to invalid IL or missing references)
		//IL_00d2: Unknown result type (might be due to invalid IL or missing references)
		//IL_00d7: Unknown result type (might be due to invalid IL or missing references)
		//IL_00dc: Unknown result type (might be due to invalid IL or missing references)
		//IL_015b: Unknown result type (might be due to invalid IL or missing references)
		if (emptyClickCreatesNew && movePainting == null)
		{
			PaintInfo paintInfo = Instantiate(painting);
            paintInfo.gameObject.transform.position = this.gameObject.transform.position;
			float num = (wallDir == Direction.V) ? ((side != 0) ? (-90f) : 90f) : ((side != 0) ? 0f : 180f);
			Vector3 rotation = new Vector3(0.0f,num);
			paintInfo.SetWallInfo(gc, ws, side, rotation);
			paintInfo.RandomiseInfo();
		}
		else if (movePainting != null)
		{
			PaintInfo paintInfo2 = movePainting;
            paintInfo2.gameObject.transform.position = this.gameObject.transform.position;
			float num2 = (wallDir == Direction.V) ? ((side != 0) ? (-90f) : 90f) : ((side != 0) ? 0f : 180f);
			Vector3 rotation2 = new Vector3(0.0f,num2);
			paintInfo2.ClearWallInfo();
			paintInfo2.SetStorage(false);
			paintInfo2.SetWallInfo(gc, ws, side, rotation2);
		}
		gc.SetGameState(GameState.PlacePainting);
	}

	private void Start()
	{
	}

	private void Update()
	{
	}

	private void OnDestroy()
	{
		gc.GameStateChanged -= On_GameStateChanged;
	}

	public void SetUp(WallScript ws, GameController gc, WallSide side)
	{
		//IL_0051: Unknown result type (might be due to invalid IL or missing references)
		this.side = side;
		this.ws = ws;
		wallDir = ws.dir;
		x = ws.x;
		y = ws.y;
		this.gc = gc;
		this.gc.GameStateChanged += On_GameStateChanged;
		this.gameObject.SetActive(false);
	}

	private void On_GameStateChanged(object sender, GameStateChangeArgs e)
	{
		//IL_001d: Unknown result type (might be due to invalid IL or missing references)
		bool flag = e.Next == GameState.PlacePainting;
		bool flag2 = ws.CanHavePainting(side);
		this.gameObject.SetActive(flag && flag2);
		movePainting = e.Painting;
	}
}
