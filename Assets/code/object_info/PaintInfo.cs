// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// PaintInfo
using System;
using TMPro;
using UnityEngine;

public class PaintInfo : MonoBehaviour
{

    public bool goldForScore;
	private static System.Random rnd = new System.Random();
	public float minPercent = 0.5f;
	public float deteriorationPercentPerSecond = 1E-05f;
	public float multiplier = 1f;
	public float currentPercent = 1f;
	public float payoffFreq = 60f;
	public GameObject progressBar;
	public float currentPoints;

    public Transform viewPoint;

    private bool inStorage;
	public int score;
	public Color cArtist;
	public string nArtist;
	public Color cStyle;
	public string nStyle;
	public Color cSubject;
	public string nSubject;
	public TextMeshPro textDisplay;
	public MeshRenderer artistDisp;
	public MeshRenderer styleDisp;
	public MeshRenderer subjectDisp;
	private GameController gc;
	private Vector3 rotated;
	private WallSide side;
	public WallScript ws;
	private bool selling;



	private void Start()
	{
	}

	private void Update()
	{

		float deltaTime = Time.deltaTime;
		float num = deteriorationPercentPerSecond * deltaTime;
		currentPercent = Math.Max(0f, currentPercent - num);
        Vector3 localScale = progressBar.transform.localScale;
		progressBar.transform.localScale =new Vector3(Math.Max(currentPercent, 0f), localScale.y, localScale.z);
        if (goldForScore)
        {
            float num2 = (currentPercent * (1f - minPercent) + minPercent) * (float)score * multiplier * deltaTime;
            currentPoints += num2;
            if (InventoryManager.im != null && currentPoints > InventoryManager.im.scorePerCoin)
            {
                currentPoints -= InventoryManager.im.scorePerCoin;
                InventoryManager.im.AddMoney(1);
            }
        }
	}

	public void OnMouseDown()
	{
		gc.MovePainting(this);
	}

    public Vector3 GetViewPoint() { return viewPoint.position; }

	public void SetInfo(int score, Color cArtist, string nArtist, Color cStyle, string nStyle, Color cSubject, string nSubject)
	{

		this.score = score;
		this.cArtist = cArtist;
		this.nArtist = nArtist;
		this.cStyle = cStyle;
		this.nStyle = nStyle;
		this.cSubject = cSubject;
		this.nSubject = nSubject;
		artistDisp.material.color =cArtist;
		styleDisp.material.color =cStyle;
		subjectDisp.material.color =cSubject;
		textDisplay.text =score.ToString();
	}

	public void ClearWallInfo()
	{

		if (ws != null)
		{
			ws.RemovePainting(side);
			this.transform.Rotate(rotated * -1f);
			ws = null;
			rotated = Vector3.zero;
		}
	}

	public void SetWallInfo(GameController gc, WallScript ws, WallSide side, Vector3 rotation)
	{
		this.gc = gc;
		this.ws = ws;
		this.side = side;
		rotated = rotation;
		this.transform.Rotate(rotation);
		ws.SetPainting(side, this);
	}

	public void RandomiseInfo()
	{
		PaintTypes pt = PaintTypes.pt;
		int num = rnd.Next(1, pt.maxScore + 1);
        RandomiseInfoSetScore(num);
	}

    public void RandomiseInfoSetScore(int score)
    {
        PaintTypes pt = PaintTypes.pt;
        int num2 = rnd.Next(0, pt.artists.Length);
        int num3 = rnd.Next(0, pt.styles.Length);
        int num4 = rnd.Next(0, pt.subject.Length);
        SetInfo(score, pt.colours[num2], pt.artists[num2], pt.colours[num3], pt.styles[num3], pt.colours[num4], pt.subject[num4]);
    }

	public void SetBeingSold(bool selling)
	{
		this.selling = selling;
	}

	public void SetStorage(bool store)
	{
		if (store)
		{
			this.gameObject.SetActive(false);
			inStorage = true;
			ClearWallInfo();
		}
		else
		{
			this.gameObject.SetActive(true);
			if (inStorage)
			{
				InventoryManager.im.RemovePainting(this);
			}
			inStorage = false;
		}
	}

    public bool GetStorage() { return inStorage; }

	public void SetMultiplier(float n)
	{
		multiplier = n;
	}

	public void RegainDeterioration(float time)
	{
		float num = deteriorationPercentPerSecond * time;
		currentPercent = Math.Min(1f, currentPercent + num);
	}
}
