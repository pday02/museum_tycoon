// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// WallScript
using System.Linq;
using UnityEngine;

public class WallScript : MonoBehaviour
{
	private Transform trans;

	private DoorCreateButton doorCreate;

	private WallCreateButton wallCreate;

	public PaintingPlaceButton pButtonf;

	public PaintingPlaceButton pButtonb;

	public bool edge;

	public int x;

	public int y;

	public Direction dir;

	public FloorPlan floorPlan;

	public DoorScript door;

	public PaintInfo pFront;

	public PaintInfo pBack;

	public WallDestroyButton wd;

	private void Start()
	{
		trans = this.GetComponent<Transform>();
	}

	private void Update()
	{
	}

	private void OnDestroy()
	{
		floorPlan.UnRegWall(this);
		Object.Destroy(pButtonf);
		Object.Destroy(pButtonb);
		if (!edge)
		{
			wallCreate.ReActivate();
		}
	}

	public bool CanBeDoor()
	{
		//IL_000c: Unknown result type (might be due to invalid IL or missing references)
		return !edge && this.gameObject.activeSelf && door == null && pFront == null && pBack == null;
	}

	public void SetPainting(WallSide side, PaintInfo p)
	{
		if (side == WallSide.F)
		{
			pFront = p;
		}
		else
		{
			pBack = p;
		}
 
	}

	public void RemovePainting(WallSide side)
	{
		if (side == WallSide.F)
		{
			pFront = null;
		}
		else
		{
			pBack = null;
		}
	}

	public bool CanHavePainting(WallSide side)
	{
		if (door == null && ((side == WallSide.F && pFront == null) || (side == WallSide.B && pBack == null)))
		{
			if (!edge)
			{
				return true;
			}
			if (y == 0 && dir == Direction.H && side == WallSide.F)
			{
				return false;
			}
			if (y != 0 && dir == Direction.H && side == WallSide.B)
			{
				return false;
			}
			if (x == 0 && dir == Direction.V && side == WallSide.B)
			{
				return false;
			}
			if (x != 0 && dir == Direction.V && side == WallSide.F)
			{
				return false;
			}
			return true;
		}
		return false;
	}

	public void SetUp(int[] start, Direction d, bool edge, FloorPlan fp, GameController gc, WallCreateButton wc)
	{
		Start();
		wallCreate = wc;
		doorCreate = this.GetComponentInChildren<DoorCreateButton>();
		floorPlan = fp;
		x = start[0];
		y = start[1];
		dir = d;
		this.edge = edge;
		floorPlan.RegWall(this);
		wd = this.GetComponentInChildren<WallDestroyButton>();
		if (edge)
		{
			Object.Destroy(wd.gameObject);
		}
		else
		{
			wd.SetUp(gc, this);
		}
		float[] array = (from x in start
		select (float)x).ToArray();
		float num = (d != Direction.V) ? 90f : 0f;
		Vector3 val = new Vector3(0f,num);
		if (d == Direction.V)
		{
			array[1] += 0.5f;
		}
		else
		{
			array[0] += 0.5f;
		}
		trans.Rotate(val);
		trans.position = new Vector3(array[0], 0f, array[1]);
		doorCreate.SetUp(gc, floorPlan, x, y, dir);
		pButtonf.SetUp(this, gc, WallSide.F);
		pButtonb.SetUp(this, gc, WallSide.B);
	}

	public void ReplaceWithDoor(DoorScript d)
	{
		door = d;
		doorCreate.gameObject.SetActive(false);
		this.gameObject.SetActive(false);
	}

	public void RemoveDoor()
	{
		door = null;
		this.gameObject.SetActive(true);
	}
}
