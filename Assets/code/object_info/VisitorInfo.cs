﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;
using System;

public class VisitorInfo : MonoBehaviour {

    public NavMeshAgent agent;
    public NavMeshObstacle obstacle;
    public float viewChance;

    private System.Random r = new System.Random();
    private List<Transform> startPath;
    private List<PaintInfo> paintings;
    private List<Transform> endPath;
    private InventoryManager im;
    private int timeSpeed;
    //private float agentBaseSpeed, agentBaseTurnSpeed,agentBaseAcceleation;

    private bool paid = false;
    public VisAction currentAction;
    public bool viewedPainting = false;
    public bool startViewPainting = false;
    public bool hasPath;
    private PaintInfo visitPaiting;
    private Vector3 paintPosition;

    private List<PaintInfo> paintingsViewed;
    private float walkHeight;
    public float viewingTime;
    private float viewTimeRemaining;
    // Use this for initialization
    void Start () {
        agent.avoidancePriority = 0;
        TimeManager.tm.MinuteChanged += OnMinute;
        OnMinute(null,null);
        paintingsViewed = new List<PaintInfo>();
        //agentBaseSpeed = agent.speed;
        //agentBaseTurnSpeed = agent.angularSpeed;
        //agentBaseAcceleation = agent.acceleration;
        UpdateAgentSpeed();
        TimeManager.tm.SpeedChange += OnSpeedChange;
	}

    private void UpdateAgentSpeed()
    {
        timeSpeed = (int) TimeManager.tm.GetSpeed();
        //agent.speed = timeSpeed * agentBaseSpeed;
        //agent.angularSpeed = timeSpeed * agentBaseTurnSpeed;
        //agent.acceleration = timeSpeed * agentBaseAcceleation;
    }

    private void OnDestroy()
    {
        TimeManager.tm.MinuteChanged -= OnMinute;
        TimeManager.tm.SpeedChange -= OnSpeedChange;
    }

    // Update is called once per frame
    void Update () {
        hasPath = agent.hasPath;

        if (currentAction == VisAction.ViewingPaiting && visitPaiting != null)
        {
            viewTimeRemaining -= Time.deltaTime;
            if (viewTimeRemaining < 0.0f) FinishViewPainting();
        }
        else if (!agent.hasPath)
        {
            if (startPath.Count > 0)
            {
                currentAction = VisAction.GoToEntrance;
                agent.SetDestination(startPath[0].position);
                startPath.RemoveAt(0);
            } else if (!paid)
            {
                paid = true;
                im.PayEntranceFee();
            } else if (currentAction == VisAction.GoToPaiting && (visitPaiting == null || visitPaiting.GetStorage() || visitPaiting.GetViewPoint() != paintPosition))

                {
                ResetPainting();
                }

            else if (visitPaiting == null && paintings.Count > 0)
            {
                currentAction = VisAction.GoToPaiting;
                visitPaiting = paintings[0]; //TODO: Pick the closest painting
                paintings.RemoveAt(0);
                paintPosition = visitPaiting.GetViewPoint();
                agent.SetDestination(visitPaiting.GetViewPoint());
            } else if (visitPaiting != null)
            {
                if (Vector2.Distance(new Vector2 (visitPaiting.GetViewPoint().x, visitPaiting.GetViewPoint().z), new Vector2(this.transform.position.x, this.transform.position.z)) > 0.5f)
                {
                    agent.SetDestination(visitPaiting.GetViewPoint());
                }
                else if (!startViewPainting)
                {
                    Debug.Log(currentAction);
                    currentAction = VisAction.ViewingPaiting;
                    startViewPainting = true;
                    ViewPainting(visitPaiting);
                }else if (viewedPainting)
                {
                    visitPaiting = null;
                    startViewPainting = false;
                }
            } else if (endPath.Count > 0)
            {
                currentAction = VisAction.GoToExit;
                agent.SetDestination(endPath[0].transform.position);
                endPath.RemoveAt(0);
            } else
            {
                Destroy(this.gameObject);
            }
        } else
        {
            if (currentAction == VisAction.GoToPaiting && (visitPaiting == null || visitPaiting.GetStorage() || visitPaiting.GetViewPoint() != paintPosition))
            {
                //todo: listeners would make this a lot more efficient to trigger changes.
                ResetPainting();
            }
        }

	}

    private void ResetPainting()
    {
        visitPaiting = null;
        startViewPainting = false;
        agent.ResetPath();
        currentAction = VisAction.AwaitingInstruction;
    }

    private void ViewPainting(PaintInfo p)
    {
        Debug.Log("Viewing!");
        //move to viewing spot
        this.gameObject.transform.position = p.GetViewPoint();
        //face painting
        this.gameObject.transform.LookAt(p.gameObject.transform);
        this.gameObject.transform.position = new Vector3(this.transform.position.x, walkHeight, this.transform.position.z);
        //lock down as a permanent obstacle that cant be moved
        agent.enabled = false;
        obstacle.enabled = true;
        //Set time to look at painting
        Debug.Log(viewingTime);
        viewTimeRemaining = viewingTime;
        
    }

    private void FinishViewPainting()
    {
        startViewPainting = false;
        paintingsViewed.Add(visitPaiting);
        visitPaiting = null;
        viewedPainting = true;
        obstacle.enabled = false;
        agent.avoidancePriority = 0;
        agent.enabled = true;
    }

    public void SetUp(InventoryManager im)
    {
        this.im = im;
    }

    public void PathAndPaintings(List<Transform> start, List<PaintInfo> paintings, List<Transform> end)
    {
        startPath = start;
        
        this.paintings = paintings.Where(x => r.NextDouble() < viewChance).ToList();
        endPath = end;
        //walkHeight = this.gameObject.transform.position.y;
    }

    private void OnMinute(object o,GameTime g)
    {
        if (agent.avoidancePriority == 99) {
            agent.avoidancePriority = agent.avoidancePriority - r.Next(5, 20);
        } else { agent.avoidancePriority++; }
    }

    private void OnSpeedChange(object o, float s)
    {
        UpdateAgentSpeed();
    }
}

public enum VisAction { GoToPaiting, AwaitingInstruction, GoToEntrance, ViewingPaiting, GoToExit}