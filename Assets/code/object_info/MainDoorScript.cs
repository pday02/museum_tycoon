// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// MainDoorScript
using UnityEngine;

public class MainDoorScript : MonoBehaviour
{
	private Transform trans;

    public Transform enterPoint, exitPoint;
    public VisitorEnterManager vis;


	private void Start()
	{
		trans = this.GetComponent<Transform>();
        vis.SetEnterLeaveMuseum(enterPoint,exitPoint);
	}

	private void Update()
	{
	}

	public void PlaceAt(int start,VisitorEnterManager vsm)
	{
        vis = vsm;
		Start();
		trans.position = new Vector3((float)start + 1f, 0f, 0f);
	}

}
