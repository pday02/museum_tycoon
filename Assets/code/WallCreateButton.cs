// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// WallCreateButton
using System.Linq;
using UnityEngine;

public class WallCreateButton : MonoBehaviour
{
	private Transform trans;

	private int[] position;

	private Direction d;

	private WallScript ws;

	private GameController gc;

	private FloorPlan floorPlan;


	private void Start()
	{
		trans = this.GetComponent<Transform>();
	}

	private void OnMouseDown()
	{
		//IL_0044: Unknown result type (might be due to invalid IL or missing references)
		WallScript wallScript = Instantiate(ws);
		wallScript.SetUp(position, d, false, floorPlan, gc, this);
		gc.GameStateChanged -= On_GameStateChanged;
		this.gameObject.SetActive(false);
	}

	public void ReActivate()
	{
		gc.GameStateChanged += On_GameStateChanged;
	}

	public void SetUp(WallScript ws, GameController gc, FloorPlan fp)
	{
		//IL_002d: Unknown result type (might be due to invalid IL or missing references)
		this.ws = ws;
		this.gc = gc;
		floorPlan = fp;
		this.gc.GameStateChanged += On_GameStateChanged;
		this.gameObject.SetActive(false);
	}

	private void On_GameStateChanged(object sender, GameStateChangeArgs e)
	{
		//IL_002d: Unknown result type (might be due to invalid IL or missing references)
		bool flag = e.Next == GameState.PlaceWalls;
		bool flag2 = floorPlan.ValidWallPlacement(position[0], position[1], d);
		this.gameObject.SetActive(flag && flag2);
	}

	public void PlaceAt(int[] position, Direction d)
	{
		//IL_0072: Unknown result type (might be due to invalid IL or missing references)
		this.position = position;
		this.d = d;
		Start();
		float[] array = (from x in position
		select (float)x).ToArray();
		if (d == Direction.V)
		{
			array[1] += 0.5f;
		}
		else
		{
			array[0] += 0.5f;
		}
		trans.position = new Vector3(array[0], 0f, array[1]);
	}
}
