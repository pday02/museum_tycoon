// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// StorageButton
using UnityEngine;
using UnityEngine.UI;

public class StorageButton : MonoBehaviour
{
	public Button button;

	private GameController gc;

	private GameStateChangeArgs currentState;

	private InventoryManager im;

	private void Start()
	{
		gc = GameController.gc;
		currentState = new GameStateChangeArgs
		{
			Next = gc.GetGameState()
		};
		gc.GameStateChanged += On_GameStateChanged;
	}

	public void OnClickAction()
	{
		im = InventoryManager.im;
		if (currentState.Next == GameState.PlacePainting && currentState.Painting != null)
		{
			currentState.Painting.SetStorage(true);
			im.StorePainting(currentState.Painting);
			gc.SetGameState(GameState.None);
		}
		else if (currentState.Next == GameState.PlacePainting)
		{
			Debug.Log((object)"Opening up storage");
		}
		else
		{
			Debug.Log((object)"Changing GameState to placing a painting and opening up storage");
		}
	}

	private void On_GameStateChanged(object sender, GameStateChangeArgs e)
	{
		currentState = e;
	}
}
