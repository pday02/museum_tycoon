// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// AuctionItemDisp
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AuctionItemDisp : MonoBehaviour
{
	public Image artistIm;

	public Image styleIm;

	public Image subjectIm;

	public TextMeshProUGUI artistTxt;

	public TextMeshProUGUI styleTxt;

	public TextMeshProUGUI subjectTxt;

	public TextMeshProUGUI scoreTxt;

	public void DisplayPainting(PaintInfo p)
	{
		//IL_000d: Unknown result type (might be due to invalid IL or missing references)
		//IL_001e: Unknown result type (might be due to invalid IL or missing references)
		//IL_0030: Unknown result type (might be due to invalid IL or missing references)
		//IL_0041: Unknown result type (might be due to invalid IL or missing references)
		//IL_0052: Unknown result type (might be due to invalid IL or missing references)
		if (p == null)
		{
			this.gameObject.SetActive(false);
		}
		else
		{
			this.gameObject.SetActive(true);
			artistIm.color =p.cArtist;
			styleIm.color =p.cStyle;
			subjectIm.color =p.cSubject;
			if (artistTxt != null)
			{
				artistTxt.text =p.nArtist;
				styleTxt.text =p.nStyle;
				subjectTxt.text =p.nSubject;
			}
			scoreTxt.text =p.score.ToString();
		}
	}
}
