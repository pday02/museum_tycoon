// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// MultCalSimpl
using System.Collections.Generic;
using System.Linq;

public class MultCalSimpl : IMultCalc
{
	public float GetMultiplier(RoomInfo room)
	{
		PaintInfo[] source = room.paintings.ToArray();
		if (source.Count() == 1)
		{
			return 1f;
		}
		bool flag = (from x in source
		select x.nArtist).Distinct().Count() == 1;
		bool flag2 = (from x in source
		select x.nStyle).Distinct().Count() == 1;
		bool flag3 = (from x in source
		select x.nSubject).Distinct().Count() == 1;
		return ((!flag) ? 1f : 2f) * ((!flag2) ? 1f : 2f) * ((!flag3) ? 1f : 2f);
	}

	public string GetRoomDesc(RoomInfo room)
	{
		string text = "Generic Room";
		PaintInfo[] source = room.paintings.ToArray();
		if (source.Count() == 0)
		{
			return "Empty Room";
		}
		if (source.Count() == 1)
		{
			return text;
		}
		IEnumerable<string> source2 = (from x in source
		select x.nArtist).Distinct();
		IEnumerable<string> source3 = (from x in source
		select x.nStyle).Distinct();
		IEnumerable<string> source4 = (from x in source
		select x.nSubject).Distinct();
		string empty = string.Empty;
		empty = ((source2.Count() <= 1 || source3.Count() <= 1 || source4.Count() <= 1) ? ("Room of " + ((source2.Count() != 1) ? string.Empty : (" " + source2.ElementAt(0) + " ")) + ((source3.Count() != 1) ? string.Empty : (" " + source3.ElementAt(0) + " ")) + ((source4.Count() != 1) ? string.Empty : (" " + source4.ElementAt(0) + " "))) : text);
		string str = "Mult: " + GetMultiplier(room).ToString();
		return empty + "\n" + str;
	}

	private void Start()
	{
	}

	private void Update()
	{
	}
}
