// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// RoomManager
using System.Collections.Generic;
using UnityEngine;

public class RoomManager : MonoBehaviour
{
	public static RoomManager rm;

	private FloorPlan fp;

	private GameController gc;

	public Color defaultTileColour;

	public Color[] colours;

	private IMultCalc multCalc = new MultCalSimpl();

	public RoomInfoDisplay display;

	private void Start()
	{
		rm = this;
	}

	private void Update()
	{
	}

	public void SetFloorPlan(FloorPlan fp)
	{
		this.fp = fp;
		gc = GameController.gc;
		gc.GameStateChanged += On_GameStateChanged;
	}

    public FloorPlan GetFloorPlan()
    {
        return fp;
    }

	private void On_GameStateChanged(object sender, GameStateChangeArgs e)
	{
		//IL_0074: Unknown result type (might be due to invalid IL or missing references)
		//IL_00ff: Unknown result type (might be due to invalid IL or missing references)
		List<RoomInfo> list = CalculateRooms();
        fp.SetRooms(list);
		if (e.Next == GameState.ShowRooms)
		{
			for (int i = 0; i < list.Count; i++)
			{
				RoomInfo roomInfo = list[i];
				string roomDesc = multCalc.GetRoomDesc(roomInfo);
				RoomInfoDisplay roomInfoDisplay = Instantiate(display);
				roomInfoDisplay.DisplayString(roomDesc, roomInfo, gc);
				foreach (RoomTileScript tile in roomInfo.tiles)
				{
					tile.SetColour(colours[i]);
				}
			}
		}
		else
		{
			if (e.Previous == GameState.ShowRooms)
			{
				RoomTileScript[,] tiles = fp.tiles;
				int length = tiles.GetLength(0);
				int length2 = tiles.GetLength(1);
				for (int j = 0; j < length; j++)
				{
					for (int k = 0; k < length2; k++)
					{
						RoomTileScript roomTileScript = tiles[j, k];
						roomTileScript.SetColour(defaultTileColour);
					}
				}
			}
			foreach (RoomInfo item in list)
			{
				float mults = multCalc.GetMultiplier(item);
				item.paintings.ForEach(delegate(PaintInfo x)
				{
					x.SetMultiplier(mults);
				});
			}
		}
	}

	private List<RoomInfo> CalculateRooms()
	{
		//IL_0015: Unknown result type (might be due to invalid IL or missing references)
		int num = fp.x / 2 - 1;
		int num2 = 0;
		RoomCalcSupport roomCalcSupport = RoomAux(new RoomCalcSupport(new Vector2Int(num, num2), fp));
		return roomCalcSupport.otherRoomCalcs;
	}

	private RoomCalcSupport RoomAux(RoomCalcSupport cur)
	{

		if (cur.toSearch.Count == 0)
		{
			if (cur.thisRoomInfo.tiles.Count > 0)
			{
				cur.otherRoomCalcs.Add(cur.thisRoomInfo);
			}
			cur.thisRoomInfo = new RoomInfo();
			if (cur.otherRooms.Count == 0)
			{
				return cur;
			}
			cur.toSearch.Add(cur.otherRooms[0]);
			cur.otherRooms.RemoveAt(0);
			return RoomAux(cur);
		}
		bool[,] searched = cur.searched;
		Vector2Int val = cur.toSearch[0];
		int x = val.x;
		Vector2Int val2 = cur.toSearch[0];
		if (searched[x, val2.y])
		{
			cur.toSearch.RemoveAt(0);
			return RoomAux(cur);
		}
		Vector2Int val3 = cur.toSearch[0];
		cur.toSearch.RemoveAt(0);
		cur.searched[val3.x, val3.y] = true;
		cur.thisRoomInfo.tiles.Add(fp.tiles[val3.x, val3.y]);
		List<PaintInfo> paintingsAtTile = fp.GetPaintingsAtTile(val3.x, val3.y);
		cur.thisRoomInfo.paintings.AddRange(paintingsAtTile);
		List<Vector2Int> tilesThroughDoor = fp.GetTilesThroughDoor(val3.x, val3.y);
		cur.otherRooms.AddRange(tilesThroughDoor);
		List<Vector2Int> connectingTiles = fp.GetConnectingTiles(val3.x, val3.y);
		cur.toSearch.AddRange(connectingTiles);
		return RoomAux(cur);
	}
}
