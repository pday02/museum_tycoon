// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// InventoryManager
using System;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{
	public static InventoryManager im;
	public int money;
	public List<PaintInfo> storedPaintings;
	public float scorePerCoin = 10f;
	public event EventHandler PaintStorageChanged;
    public int entryCost = 10;
    public RoomManager rm;
    //private FloorPlan fp;

	private void Start()
	{
		im = this;
        //fp = rm.GetFloorPlan();
		storedPaintings = new List<PaintInfo>();
	}

	private void Update()
	{
		storedPaintings.ForEach(delegate(PaintInfo x)
		{
			x.RegainDeterioration(Time.deltaTime);
		});
	}

	public void StorePainting(PaintInfo p)
	{
		storedPaintings.Add(p);
		OnPaintStorageChanged(EventArgs.Empty);
	}

    public List<PaintInfo> GetDisplayedPaintings()
    {
        return rm.GetFloorPlan().GetPaintings();
    }

    public void PayEntranceFee()
    {
        money += entryCost;
    }

	public PaintInfo RemovePainting(int i)
	{
		PaintInfo result = storedPaintings[i];
		storedPaintings.RemoveAt(i);
		OnPaintStorageChanged(EventArgs.Empty);
		return result;
	}

	public void RemovePainting(PaintInfo pi)
	{
		storedPaintings.Remove(pi);
		OnPaintStorageChanged(EventArgs.Empty);
	}

	public void AddMoney(int n)
	{
		money += n;
	}

	public bool RemoveMoney(int n)
	{
		if (n < money)
		{
			money -= n;
			return true;
		}
		return false;
	}

	protected virtual void OnPaintStorageChanged(EventArgs e)
	{
		if (this.PaintStorageChanged != null)
		{
			this.PaintStorageChanged(this, e);
		}
	}
}
