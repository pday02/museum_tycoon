﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisitorEnterManager : MonoBehaviour {

    public VisitorInfo[] visitorTypes;
    public InventoryManager im;
    public Transform pointOnOffMap;
    public TimeManager tm;
    private Transform pointEnterMuseum;
    private Transform pointLeaveMuseum;

    public int openingHour, closingHour;
    private float arrivalProb;
    private System.Random r = new System.Random();

	// Use this for initialization
	void Start () {
        tm.MinuteChanged += OnMinute;
        tm.DayChanged += OnDay;
        arrivalProb = 0.05f;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetEnterLeaveMuseum(Transform enter, Transform leave)
    {
        pointEnterMuseum = enter;
        pointLeaveMuseum = leave;
    }

    public void AddVisitor()
    {
        VisitorInfo v = Instantiate(ArraySupport.GetRandom(visitorTypes));
        
        v.gameObject.transform.position = pointOnOffMap.position;
        v.SetUp(im);
        v.PathAndPaintings(new List<Transform>() { pointEnterMuseum }, im.GetDisplayedPaintings(), new List<Transform>() { pointLeaveMuseum, pointOnOffMap });

    }

    private void OnMinute(object sender, GameTime t)
    {
        if (t.Hour >= openingHour && t.Hour < closingHour)
        {
            if (r.NextDouble() < arrivalProb) AddVisitor();
        }
    }

    private void OnDay(object sender, GameTime t)
    {
        //Todo, add calculation for ammount
        //TODO: Collect reviews
    }
}
