﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour {
    public static TimeManager tm;
    public float gameMinuteLengthSeconds;
    public int startHour;
    public int startMinute;
    private int day;
    private int hour;
    private int minute;
    private float timeThisMinute=0.0f;
    private float speed = 1.0f;

	// Use this for initialization
	void Start () {
        tm = this;
        hour = startHour;
        minute = startMinute;
	}
	
	// Update is called once per frame
	void Update () {
        timeThisMinute += Time.deltaTime * speed;
        if (timeThisMinute > gameMinuteLengthSeconds)
        {
            timeThisMinute -= gameMinuteLengthSeconds;
            AddMinute();
        }
	}

    public float GetSpeed()
    {
        return speed;
    }

    public void SetSpeed(float speed)
    {
        if (this.speed != speed)
        {
            this.speed = speed;
            SpeedChange?.Invoke(this, speed);
        }
    }

    private void AddMinute()
    {
        if (minute == 59)
        {
            minute = 0;
            AddHour();
        }
        else minute++;
        MinuteChanged?.Invoke(this, GetTime());
    }

    private void AddHour()
    {
        if (hour == 23)
        {
            hour = 0;
            AddDay();
        }
        else hour++;
        HourChanged?.Invoke(this, GetTime());
    }

    private void AddDay()
    {
        day++;
        DayChanged?.Invoke(this, GetTime());
    }

    public GameTime GetTime()
    {
        return new GameTime(day, hour, minute);
    } 

    public event EventHandler<GameTime> HourChanged;
    public event EventHandler<GameTime> MinuteChanged;
    public event EventHandler<GameTime> DayChanged;
    public event EventHandler<float> SpeedChange; 

}

public class GameTime
{
    public GameTime(int d, int h, int m)
    {
        Day = d;
        Hour = h;
        Minute = m;
    }
    public int Day { get; }
    public int Minute { get; }
    public int Hour { get; }
}
