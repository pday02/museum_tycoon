// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// AuctionManager
using System;
using UnityEngine;

public class AuctionManager : MonoBehaviour
{
	private System.Random rnd = new System.Random();

	private InventoryManager im;

	public float roundTime;

	public float timeStartBidding;

	private float timeNextBid;

	public PaintInfo paintPrefab;

	public float timeRem;

	public int maxBid;

	private PaintInfo b1;

	private PaintInfo b2;

	private PaintInfo b3;

	private PaintInfo bL;

	private PaintInfo s1;

	private PaintInfo s2;

	private PaintInfo s3;

	private PaintInfo sL;

	private int timeLeft;

	private int nextBidAmount;

	private int lastSell;

	private int lastBuy;

	private int thisSell;

	private int thisBuy;

	private bool playerBid;

	private int playerBidAmount;

	private bool prevPlayerBought;

	private bool sellingPlayer;

	private bool NewAuction = true;

	public event EventHandler<AuctionChangeArgs> AuctionChanged;

	private void Start()
	{
		b1 = GetNextPaint();
		b2 = GetNextPaint();
		b3 = GetNextPaint();
		timeRem = roundTime;
		timeLeft = (int)roundTime;
		nextBidAmount = thisBuy + NextBidIncrement(thisBuy);
		maxBid = GetMaxBid(b1.score);
		timeNextBid = NextBidTime();
		im = this.GetComponent<InventoryManager>();
	}

	private void Update()
	{
		bool flag = false;
		timeRem -= Time.deltaTime;
		if ((int)timeRem != timeLeft)
		{
			timeLeft = (int)timeRem;
			flag = true;
		}
		if (timeRem < timeNextBid && maxBid > ((!sellingPlayer) ? thisBuy : thisSell))
		{
			MakeAIBid();
			flag = true;
		}
		if ((double)timeRem < 0.0)
		{
			NextAuction();
			flag = true;
		}
		if (flag)
		{
			OnAuctionChanged();
		}
	}

	private void MakeAIBid()
	{
		if (playerBid)
		{
			im.AddMoney(playerBidAmount);
		}
		playerBid = false;
		MakeBid();
	}

	public bool MakePlayerBid()
	{
		if (!sellingPlayer && !playerBid && im.RemoveMoney(nextBidAmount))
		{
			playerBid = true;
			playerBidAmount = nextBidAmount;
			MakeBid();
			OnAuctionChanged();
			return true;
		}
		return false;
	}

	private void MakeBid()
	{
		timeRem = timeStartBidding;
		timeLeft = (int)timeRem;
		if (sellingPlayer)
		{
			thisSell = nextBidAmount;
		}
		else
		{
			thisBuy = nextBidAmount;
		}
		nextBidAmount = NextBidIncrement((!sellingPlayer) ? thisBuy : thisSell) + ((!sellingPlayer) ? thisBuy : thisSell);
		timeNextBid = NextBidTime();
	}

	private float NextBidTime()
	{
		return (float)rnd.NextDouble() * timeStartBidding;
	}

	private void NextAuction()
	{
		//IL_004d: Unknown result type (might be due to invalid IL or missing references)
		//IL_006e: Unknown result type (might be due to invalid IL or missing references)
		//IL_01f7: Unknown result type (might be due to invalid IL or missing references)
		NewAuction = true;
		timeRem = roundTime;
		timeLeft = (int)roundTime;
		if (!sellingPlayer)
		{
			if (!prevPlayerBought && bL != null)
			{
				Destroy(bL.gameObject);
			}
			if (sL != null)
			{
				Destroy(sL.gameObject);
			}
			if (playerBid)
			{
				b1.SetStorage(true);
				im.StorePainting(b1);
				prevPlayerBought = true;
			}
			else
			{
				prevPlayerBought = false;
			}
			bL = b1;
			b1 = b2;
			b2 = b3;
			b3 = GetNextPaint();
			sL = s1;
			s1 = s2;
			s2 = s3;
			s3 = null;
			lastBuy = thisBuy;
			lastSell = thisSell;
			thisBuy = 0;
			thisSell = 0;
			nextBidAmount = NextBidIncrement(thisBuy);
			if (s1 != null)
			{
				maxBid = GetMaxBid(s1.score);
				sellingPlayer = true;
				playerBid = false;
			}
			else
			{
				maxBid = GetMaxBid(b1.score);
				sellingPlayer = false;
				playerBid = false;
			}
		}
		else
		{
			im.AddMoney(thisSell);
			sellingPlayer = false;
			maxBid = GetMaxBid(b1.score);
			s1.ClearWallInfo();
			s1.SetStorage(false);
			s1.gameObject.SetActive(false);
			nextBidAmount = NextBidIncrement(thisBuy);
		}
	}

	public void AddNextPaintingToSell(PaintInfo p)
	{
		if (p == s3)
		{
			Remove2ndPaintingToSell();
		}
		if (p != s1)
		{
			if (s2 != null)
			{
				s2.SetBeingSold(false);
			}
			s2 = p;
			p.SetBeingSold(true);
			OnAuctionChanged();
		}
	}

	public void RemoveNextPaintingToSell()
	{
		if (s2 != null)
		{
			s2.SetBeingSold(false);
			s2 = null;
			OnAuctionChanged();
		}
	}

	public void Add2ndPaintingToSell(PaintInfo p)
	{
		if (p == s2)
		{
			RemoveNextPaintingToSell();
		}
		if (p != s1)
		{
			if (s3 != null)
			{
				s3.SetBeingSold(false);
			}
			s3 = p;
			p.SetBeingSold(true);
			OnAuctionChanged();
		}
	}

	public void Remove2ndPaintingToSell()
	{
		if (s3 != null)
		{
			s3.SetBeingSold(false);
			s3 = null;
			OnAuctionChanged();
		}
	}

	public int NextBidIncrement(int cur)
	{
		if (cur < 20)
		{
			return 1;
		}
		if (cur < 50)
		{
			return 5;
		}
		if (cur < 150)
		{
			return 10;
		}
		if (cur < 500)
		{
			return 25;
		}
		return 50;
	}

	public int GetMaxBid(int score)
	{
		return rnd.Next(10, score * 100);
	}

	public PaintInfo GetNextPaint()
	{
		//IL_0013: Unknown result type (might be due to invalid IL or missing references)
		PaintInfo paintInfo = Instantiate(paintPrefab);
		paintInfo.RandomiseInfo();
		paintInfo.gameObject.SetActive(false);
		return paintInfo;
	}

	protected virtual void OnAuctionChanged()
	{
		AuctionChangeArgs auctionChangeArgs = new AuctionChangeArgs();
		auctionChangeArgs.NewAuction = NewAuction;
		auctionChangeArgs.SellingPlayer = sellingPlayer;
		auctionChangeArgs.PlayerWinning = (!sellingPlayer && playerBid);
		auctionChangeArgs.B1 = b1;
		auctionChangeArgs.B2 = b2;
		auctionChangeArgs.B3 = b3;
		auctionChangeArgs.BL = bL;
		auctionChangeArgs.S1 = s1;
		auctionChangeArgs.S2 = s2;
		auctionChangeArgs.S3 = s3;
		auctionChangeArgs.SL = sL;
		auctionChangeArgs.TimeLeft = timeLeft;
		auctionChangeArgs.NextBidAmount = nextBidAmount;
		auctionChangeArgs.LastSell = lastSell;
		auctionChangeArgs.LastBuy = lastBuy;
		auctionChangeArgs.ThisSell = thisSell;
		auctionChangeArgs.ThisBuy = thisBuy;
		AuctionChangeArgs e = auctionChangeArgs;
		if (this.AuctionChanged != null)
		{
			this.AuctionChanged(this, e);
		}
		NewAuction = false;
	}
}
