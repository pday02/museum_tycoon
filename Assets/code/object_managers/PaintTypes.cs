// Warning: Some assembly references could not be loaded. This might lead to incorrect decompilation of some parts,
// for ex. property getter/setter access. To get optimal decompilation results, please manually add the references to the list of loaded assemblies.
// PaintTypes
using UnityEngine;

public class PaintTypes : MonoBehaviour
{
	public static PaintTypes pt;

	public string[] artists;

	public string[] styles;

	public string[] subject;

	public int maxScore;

	public Color[] colours;

	private void Start()
	{
		pt = this;
	}

	private void Update()
	{
	}
}
