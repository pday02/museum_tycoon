﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartingState : MonoBehaviour {

    public int startingMoney;
    public int numPaintings;
    public int maxPaintScore;
    public InventoryManager im;
    public PaintInfo paint;
    private System.Random ran = new System.Random();

	// Use this for initialization
	void Start () {
        im.AddMoney(startingMoney);

        for (int i=0; i < numPaintings; i++)
        {
            PaintInfo p = Instantiate(paint);
            var score = ran.Next(1, maxPaintScore);
            p.RandomiseInfoSetScore(score);
            p.SetStorage(true);
            im.StorePainting(p);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
